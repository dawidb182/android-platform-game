
package pl.majority.game.util;

import pl.majority.game.objects.AbstractGameObject;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

public class CameraHelper
{
	private Vector2 position;
	private AbstractGameObject target;

	public CameraHelper()
	{
		position = new Vector2();
	}

	public void update( float deltaTime )
	{
		if (!hasTarget())
		{
			return;
		}
		position.x = target.position.x + target.origin.x;
		position.y = target.position.y + target.origin.y;
		position.x = Math.max(5f, position.x);
		position.x = Math.min(45f, position.x);
		position.y = Math.max(3f, position.y);
		position.y = Math.min(17f, position.y);
	}

	public void setPosition( float x, float y )
	{
		this.position.set(x, y);
	}

	public Vector2 getPosition()
	{
		return position;
	}

	public void setTarget( AbstractGameObject target )
	{
		this.target = target;
	}

	public AbstractGameObject getTarget()
	{
		return target;
	}

	public boolean hasTarget()
	{
		return target != null;
	}

	public boolean hasTarget( AbstractGameObject target )
	{
		return hasTarget() && this.target.equals(target);
	}

	public void applyTo( OrthographicCamera camera )
	{
		camera.position.x = position.x;
		camera.position.y = position.y;
		camera.update();
	}
}
