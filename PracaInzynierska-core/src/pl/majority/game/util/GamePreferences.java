package pl.majority.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;

public class GamePreferences
{
	private static GamePreferences	instance = null;
	public boolean					sound;
	public boolean					music;
	public float					volSound;
	public float					volMusic;
	public String					characterSkin;

	private boolean					isInitialize;

	private final Preferences		prefs;
	public boolean drawDebug = false;
	public boolean LogFps = false;
	private GamePreferences()
	{
		this.prefs = Gdx.app.getPreferences("Majority.prefs");
		if (!prefs.getBoolean("isInitialize"))
		{
			isInitialize = true;
			this.resetToDefault();
			prefs.flush();
		}
	}

	public static GamePreferences getInstance()
	{
		if (instance == null)
		{
			instance = new GamePreferences();
		}
		return instance;
	}

	public void load()
	{
		sound = prefs.getBoolean("sound", true);
		music = prefs.getBoolean("music", true);
		volSound = MathUtils
				.clamp(prefs.getFloat("volSound"), 0.0f, 1.0f);
		volMusic = MathUtils
				.clamp(prefs.getFloat("volMusic"), 0.0f, 1.0f);
		characterSkin = prefs.getString("characterSkin");
		isInitialize = prefs.getBoolean("isInitialize");
	}

	public void savePreferences()
	{
		prefs.putBoolean("sound", sound);
		prefs.putBoolean("music", music);
		prefs.putFloat("volSound", volSound);
		prefs.putFloat("volMusic", volMusic);
		prefs.putString("characterSkin", characterSkin);
		prefs.putBoolean("isInitialize", isInitialize);
		prefs.flush();
	}

	public void resetToDefault()
	{
		prefs.putBoolean("sound", true);
		prefs.putBoolean("music", true);
		prefs.putFloat("volSound", 0.7f);
		prefs.putFloat("volMusic", 0.5f);
		prefs.putString("characterSkin", "cloth");
		prefs.putBoolean("isInitialize", true);
		prefs.flush();
	}

}
