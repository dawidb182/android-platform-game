
package pl.majority.game.util;

import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.objects.Enemy;
import pl.majority.game.objects.Enemy.ENEMY_LOOKING;
import pl.majority.game.objects.Enemy.ENEMY_TYPE;
import pl.majority.game.objects.Enemy.EnemyPosition;
import pl.majority.game.objects.FireBall;
import pl.majority.game.objects.GoldCoin;
import pl.majority.game.objects.JumpingTrampoline;
import pl.majority.game.objects.Level;
import pl.majority.game.objects.MovingPlatform;
import pl.majority.game.objects.Player;
import pl.majority.game.objects.Player.PLAYER_IS_LOOKING;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.WorldManifold;

public class MyContactListener implements ContactListener
{
	// ENEMIES AND PLAYER AND FIREBALL USER DATA ARE OBJECTS!!!!!!!! NOT string
	// (but string
	// is object too) :(
	public Player playerReference = null;
	public Enemy enemyReference = null;
	public FireBall fireBallReference = null;
	public GoldCoin goldCoinReference = null;
	public MovingPlatform movingPlatformReference = null;
	public String userDataString = null;
	public JumpingTrampoline jumpingTrampolineReference = null;

	@Override
	public void beginContact( Contact contact )
	{
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		if (a == null || b == null)
			return;
		// PLAYERS down line COLLISION WITH...
		if (a.getUserData() instanceof Player)
		{
			playerReference = (Player) a.getUserData();
			// //PLAYER <--COLLISION--> ENEMY
			if (b.getUserData() instanceof Enemy)
			{
				enemyReference = (Enemy) b.getUserData();
				if (enemyReference.isAlive())
				{
					float impulseX = contact.getWorldManifold().getNormal().x > 0 ? -3f : 3f;
					playerReference.body.applyLinearImpulse(impulseX, 0, playerReference.position.x, playerReference.position.y, true);
					if (enemyReference.enemyIsLooking.equals(ENEMY_LOOKING.LEFT))
					{
						enemyReference.enemyIsLooking = ENEMY_LOOKING.RIGHT;
					}
					else
					{
						enemyReference.enemyIsLooking = ENEMY_LOOKING.LEFT;
					}
					playerReference.lives--;
					if (playerReference.lives > 0)
					{
						if (GamePreferences.getInstance().sound)
						{
							MyAssetsManager.instance.blahblah.play(GamePreferences.getInstance().volSound);
						}
					}
				}
			}
			else
				// // //PLAYER <--COLLISION--> GOLDCOIN
				if (b.getUserData() instanceof GoldCoin)
				{
					goldCoinReference = (GoldCoin) b.getUserData();
					playerReference.score += goldCoinReference.getScore();
					goldCoinReference.collected = true;
					if (GamePreferences.getInstance().sound)
					{
						MyAssetsManager.instance.coin.play(GamePreferences.getInstance().volSound);
					}
				}
				// PLAYER <--COLLISION--> ANYTHING ELSE
				else
					if (b.getUserData() instanceof String) // TILERIGHT,
															// TILELEFT,
															// TILE
					{
						userDataString = (String) b.getUserData();
						// player with tile
						if (userDataString.equals("TILE"))
						{
							contact.setFriction(0.8f);
							WorldManifold worldManifold = contact.getWorldManifold();
							Vector2 worldNormal = worldManifold.getNormal();
							if (worldNormal.x == 1.0f || worldNormal.x == -1.0f || worldNormal.y == 1.0f)
							{
								contact.setFriction(0.0f);
							}
						} // /|
							// player with /_|
						else
							if (userDataString.equals("TILELEFT"))
							{
								contact.setFriction(0.1f);
								if ((playerReference.playerIsLooking == PLAYER_IS_LOOKING.RIGHT)
										&& (playerReference.body.getLinearVelocity().x > playerReference.maxVelocityWalking.x))
								{
									playerReference.body.applyLinearImpulse(1, 1, playerReference.body.getPosition().x, playerReference.body.getPosition().y,
											true);
								}
							}
							else
								if (userDataString.equals("TILERIGHT"))
								{
									contact.setFriction(0.1f);
									if ((playerReference.playerIsLooking == PLAYER_IS_LOOKING.LEFT)
											&& (playerReference.body.getLinearVelocity().x < -playerReference.maxVelocityWalking.x))
									{
										playerReference.body.applyLinearImpulse(-1, 1, playerReference.body.getPosition().x,
												playerReference.body.getPosition().y, true);
									}
								}
					}
					// PLAYER <--COLLISION--> FINAL POINT
					else
						if (b.getUserData() instanceof Level)
						{
							Level level = (Level) b.getUserData();
							if (level.isLevelEmpty())
							{
								level.isFinished = true;
							}
						}
						else
							if (b.getUserData() instanceof JumpingTrampoline)
							{
								jumpingTrampolineReference = (JumpingTrampoline) b.getUserData();
								if (playerReference.body.getPosition().y >= (jumpingTrampolineReference.position.y + 0.2f))
								{
									if (GamePreferences.getInstance().sound)
									{
										MyAssetsManager.instance.throwingInTrampoline.play(GamePreferences.getInstance().volSound);
									}
									jumpingTrampolineReference.setPlayerReference(playerReference);
								}
							}
							else
								if (b.getUserData() instanceof MovingPlatform)
								{
									movingPlatformReference = (MovingPlatform) b.getUserData();
									// avoid to take player from one platform to another platform
									if ((movingPlatformReference.getPlayerReference() == null) && (playerReference.getPlatformReference() == null))
									{
										if ((movingPlatformReference.body.getPosition().y + 1f) <= playerReference.body.getPosition().y)
										{
											movingPlatformReference.setPlayerReference(playerReference);
											playerReference.setPlatformReference(movingPlatformReference);
										}
									}
									else
									{
										contact.setEnabled(false);
									}
								}
		}
		// fireball with enemy
		if (a.getUserData() instanceof FireBall)
		{
			fireBallReference = (FireBall) a.getUserData();
			// With enemy ...
			if (b.getUserData() instanceof Enemy)
			{
				enemyReference = (Enemy) b.getUserData();
				enemyReference.hp -= fireBallReference.getDamage();
			}
		}
	
		// fireball with enemy
		if (a.getUserData() instanceof Enemy )
		{
			enemyReference = (Enemy) a.getUserData();
			// With enemy ...
			if (b.getUserData() instanceof FireBall)
			{
				fireBallReference = (FireBall) b.getUserData();
				enemyReference.hp -= fireBallReference.getDamage();
			}
		}
		
		//
		// goldcoins with player rect
		if (a.getUserData() instanceof GoldCoin)
		{
			goldCoinReference = (GoldCoin) a.getUserData();
			if (b.getUserData() instanceof String)
			{
				String s = (String) b.getUserData();
				if (s.equals("PLAYER_RECT"))
				{
					playerReference.score += goldCoinReference.getScore();
					goldCoinReference.collected = true;
					if (GamePreferences.getInstance().sound)
					{
						MyAssetsManager.instance.coin.play(GamePreferences.getInstance().volSound);
					}
				}
			}
		}
		// some string collide with..
		if (a.getUserData() instanceof String)
		{
			userDataString = (String) a.getUserData();
			// PLAYER RECT COLLIDE WITH:..
			if (userDataString.equals("PLAYER_RECT"))
			{
				// player rect collides with end point :)
				if (b.getUserData() instanceof Level)
				{
					Level level = (Level) b.getUserData();
					if (level.isLevelEmpty())
					{
						level.isFinished = true;
					}
				}
				else
					// ..with ENEMY
					if (b.getUserData() instanceof Enemy)
					{
						enemyReference = (Enemy) b.getUserData();
						if (enemyReference.isAlive())
						{
							float impulseX = contact.getWorldManifold().getNormal().x > 0 ? -4f : 4f;
							playerReference.body.applyLinearImpulse(impulseX, 0, playerReference.position.x, playerReference.position.y, true);
							if (enemyReference.enemyIsLooking.equals(ENEMY_LOOKING.LEFT))
							{
								enemyReference.enemyIsLooking = ENEMY_LOOKING.RIGHT;
							}
							else
							{
								enemyReference.enemyIsLooking = ENEMY_LOOKING.LEFT;
							}
							playerReference.lives--;
							if (playerReference.lives > 0)
							{
								if (GamePreferences.getInstance().sound)
								{
									MyAssetsManager.instance.blahblah.play(GamePreferences.getInstance().volSound);
								}
							}
						}
					}
			}
			if (userDataString.equals("TILE") || userDataString.equals("TILELEFT") || userDataString.equals("TILERIGHT"))
			{
				if (b.getUserData() instanceof Enemy)
				{
					enemyReference = (Enemy) b.getUserData();
					WorldManifold worldManifold = contact.getWorldManifold();
					Vector2 worldNormal = worldManifold.getNormal();
					if (worldNormal.x == 1.0f || worldNormal.x == -1.0f)
					{
						if (enemyReference.enemyIsLooking == ENEMY_LOOKING.LEFT)
						{
							enemyReference.enemyIsLooking = ENEMY_LOOKING.RIGHT;
						}
						else
							if (enemyReference.enemyIsLooking == ENEMY_LOOKING.RIGHT)
							{
								enemyReference.enemyIsLooking = ENEMY_LOOKING.LEFT;
							}
					}
					if (Math.round(worldNormal.y) == 1 && Math.round(worldNormal.x) == 0 && enemyReference.enemyType != ENEMY_TYPE.FLY)
					{
						enemyReference.enemyPositionType = EnemyPosition.WALKING;
					}
				}
				if (b.getUserData() instanceof String)
				{
					userDataString = (String) b.getUserData();
					if (userDataString.equals("PLAYER_RECT"))
					{
						Vector2 worldNormal = contact.getWorldManifold().getNormal();
						if (worldNormal.x == 1.0f || worldNormal.x == -1.0f)
						{
							contact.setFriction(0.0f);
						}
						else
							if (worldNormal.y == -1.0f)
							{
								contact.setFriction(0.0f);
								contact.setRestitution(0.05f);
							}
					}
				}
			}
		}
		// enemy with enemies
		if (a.getUserData() instanceof Enemy)
		{
			enemyReference = (Enemy) a.getUserData();
			if (b.getUserData() instanceof Enemy)
			{
				Enemy enemyTemp = (Enemy) b.getUserData();
				if (enemyReference.enemyIsLooking == ENEMY_LOOKING.LEFT)
				{
					enemyReference.enemyIsLooking = ENEMY_LOOKING.RIGHT;
				}
				else
					if (enemyReference.enemyIsLooking == ENEMY_LOOKING.RIGHT)
					{
						enemyReference.enemyIsLooking = ENEMY_LOOKING.LEFT;
					}
				if (enemyTemp.enemyIsLooking == ENEMY_LOOKING.LEFT)
				{
					enemyTemp.enemyIsLooking = ENEMY_LOOKING.RIGHT;
				}
				else
					if (enemyTemp.enemyIsLooking == ENEMY_LOOKING.RIGHT)
					{
						enemyTemp.enemyIsLooking = ENEMY_LOOKING.LEFT;
					}
			}
		}
	}

	@Override
	public void endContact( Contact contact )
	{
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		if (a == null || b == null)
			return;
		// powrot do normalnego tlumienia
		if (a.getUserData() instanceof Player)
		{
			playerReference = (Player) a.getUserData();
			if (b.getUserData() instanceof String)
			{
				userDataString = (String) b.getUserData();
				if (userDataString.equals("TILE")) // ((String)
													// b.getUserData()).equals("TILE")
				{
					contact.setFriction(0.8f);
				}
				if ("TILELEFT".equals(userDataString) || "TILERIGHT".equals(userDataString))
				{
					playerReference.goingStairs = 0;
					contact.setFriction(0.8f);
				}
			}
			else
				if (b.getUserData() instanceof Enemy)
				{
					contact.setFriction(0.8f);
				}
				else
					if (b.getUserData() instanceof JumpingTrampoline)
					{
						jumpingTrampolineReference = (JumpingTrampoline) b.getUserData();
						jumpingTrampolineReference.setPlayerReference(null);
					}
					else
						if (b.getUserData() instanceof MovingPlatform)
						{
							movingPlatformReference = (MovingPlatform) b.getUserData();
							movingPlatformReference.setPlayerReference(null);
							playerReference.setPlatformReference(null);
						}
		}
		// player rect ends collide with..
		if (a.getUserData() instanceof String)
		{
			userDataString = (String) a.getUserData();
			if (userDataString.equals("PLAYER_RECT"))
			{
				if (b.getUserData() instanceof String)
				{
					userDataString = (String) b.getUserData();
					if (userDataString.equals("TILE") || userDataString.equals("TILELEFT") || userDataString.equals("TILERIGHT"))
					{
						contact.setFriction(0.8f);
					}
				}
			}
			// make enemies, without fly, falling
			if (userDataString.equals("TILE") || userDataString.equals("TILELEFT") || userDataString.equals("TILERIGHT"))
			{
				if (b.getUserData() instanceof Enemy)
				{
					enemyReference = (Enemy) b.getUserData();
					if (!enemyReference.enemyType.equals(ENEMY_TYPE.FLY))
					{
						WorldManifold worldManifold = contact.getWorldManifold();
						Vector2 worldNormal = worldManifold.getNormal();
						if (Math.abs(Math.round(worldNormal.y)) == 1 && Math.abs(Math.round(-worldNormal.x)) == 1)
						{
							enemyReference.enemyPositionType = EnemyPosition.FALLING;
						}
					}
				}
			}
		}
	}

	@Override
	public void preSolve( Contact contact, Manifold oldManifold )
	{
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		if (a == null || b == null)
			return;
		if (a.getUserData() instanceof Player)
		{
			playerReference = (Player) a.getUserData();
			if (b.getUserData() instanceof String)
			{
				userDataString = (String) b.getUserData();
				switch (userDataString)
				{
					case "TILE" :
						playerReference.goingStairs = 0;
						break;
					case "TILELEFT" :
						float x = a.getBody().getLinearVelocity().x;
						if (x > 0.0f)
						{
							playerReference.goingStairs = 2;
						}
						else
							if (x < 0.0f)
							{
								playerReference.goingStairs = -1;
							}
							else
							{
								playerReference.goingStairs = 0;
							}
						break;
					case "TILERIGHT" :
						float xTemp = contact.getFixtureA().getBody().getLinearVelocity().x;
						if (xTemp > 0.0f)
						{
							playerReference.goingStairs = 1;
						}
						else
							if (xTemp < 0.0f)
							{
								playerReference.goingStairs = -2;
							}
							else
							{
								playerReference.goingStairs = 0;
							}
						break;
				}
			}
			else
				if (b.getUserData() instanceof MovingPlatform)
				{
					movingPlatformReference = (MovingPlatform) b.getUserData();
					// in case of player is standing at platform and if another platform has come
					if (playerReference.getPlatformReference() != null && playerReference.getPlatformReference() != movingPlatformReference)
					{
						contact.setEnabled(false);
					}
					Vector2 worldNormal = contact.getWorldManifold().getNormal();
					// for every diffrent collision of _\/_ disable it off
					if (!(worldNormal.x == 0 && worldNormal.y == -1)
							|| !(movingPlatformReference.body.getPosition().y + 1f <= playerReference.body.getPosition().y))
					{
						contact.setEnabled(false);
					}
					else
					{
						movingPlatformReference.setPlayerReference(playerReference);
						playerReference.setPlatformReference(movingPlatformReference);
					}
				}
		}
	}

	@Override
	public void postSolve( Contact contact, ContactImpulse impulse )
	{
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		if (a == null || b == null)
			return;
	}
}
