
package pl.majority.game.screens;

import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.objects.Player;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

public class SettingsScreen extends AbstractScreen
{
	private Stage stage;
	private Skin skin;
	private TextButton btnSave, btnBack;
	private CheckBox chkSound, chkMusic;
	private Image imagePlayerSelected;
	private Table tableOptions, rootTable;
	private Slider sldSound, sldMusic;
	private Label labelGametitle, labelAskMusic, labelAskSound, labelSetCharacter, labelMusicPercents, labelSoundPercent; // labelAskFps
																															// ,
	private SpriteBatch sb;
	private SelectBox<String> selectBoxCharacter;
	private TextureRegion gamerAtlasRegion, backgroundImage;

	public SettingsScreen( Game game )
	{
		super(game);
		skin = MyAssetsManager.instance.skin;
		sb = new SpriteBatch();
		stage = new Stage(new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		btnSave = new TextButton("Save", skin, "mainSetting");
		btnBack = new TextButton("Go back", skin, "mainSetting");
		TextureRegionDrawable buttonTextureDrawable = new TextureRegionDrawable(MyAssetsManager.instance.textures.get("mainButton"));
		btnSave.setBackground(buttonTextureDrawable);
		btnBack.setBackground(buttonTextureDrawable);
		rootTable = new Table();
		tableOptions = new Table();
		labelAskMusic = new Label("Enable music? ", skin);
		labelAskSound = new Label("Enable sound? ", skin);
		labelSetCharacter = new Label("Set character: ", skin);
		labelMusicPercents = new Label("", skin);
		labelSoundPercent = new Label("", skin);
		chkSound = new CheckBox("", skin);
		chkMusic = new CheckBox("", skin);
		sldSound = new Slider(0.0f, 1.0f, 0.1f, false, skin);
		sldMusic = new Slider(0.0f, 1.0f, 0.1f, false, skin);
		selectBoxCharacter = new SelectBox<String>(skin, "sylektBoksKaraktyr");
		selectBoxCharacter.setItems("cloth", "leather", "steel");
		loadSettingsFromFile();
		gamerAtlasRegion = MyAssetsManager.instance.textures.get("assets1/Player/" + selectBoxCharacter.getSelected() + "Character");
		backgroundImage = MyAssetsManager.instance.textures.get("backgroundImage");
		TextureRegion[][] temp = gamerAtlasRegion.split(gamerAtlasRegion.getRegionWidth() / Player.rowsInSpritesheet, gamerAtlasRegion.getRegionHeight()
				/ Player.colsInSpritesheet);
		imagePlayerSelected = new Image(new TextureRegion(temp[8][0]));
		labelMusicPercents.setText(String.valueOf(MathUtils.round(sldMusic.getValue() * 100)) + "%");
		labelSoundPercent.setText(String.valueOf(MathUtils.round(sldSound.getValue() * 100)) + "%");
		labelGametitle = new Label("Settings", skin, "labelMain");
	}

	@Override
	public void render( float deltaTime )
	{
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		sb.begin();
		sb.draw(backgroundImage, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		sb.end();
		stage.act(deltaTime);
		stage.draw();
	}

	@Override
	public void resize( int width, int height )
	{
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show()
	{
		rootTable.setFillParent(true);
		// // zmienne do debugowania
		if (GamePreferences.getInstance().drawDebug)
		{
			tableOptions.setDebug(true);
			rootTable.setDebug(true);
		}
		rootTable.add(labelGametitle).expandX().top().colspan(2).row();
		rootTable.add(tableOptions).expand().center();
		sldMusic.setVisible(false);
		sldSound.setVisible(false);
		labelMusicPercents.setVisible(false);
		labelSoundPercent.setVisible(false);
		// sprawdz podczas pierwszego wczytania czy wyswietlic slidery
		if (chkSound.isChecked())
		{
			sldSound.setVisible(true);
			labelSoundPercent.setVisible(true);
		}
		else
		{
			sldSound.setVisible(false);
			labelSoundPercent.setVisible(false);
		}
		if (chkMusic.isChecked())
		{
			sldMusic.setVisible(true);
			labelMusicPercents.setVisible(true);
		}
		else
		{
			sldMusic.setVisible(false);
			labelMusicPercents.setVisible(false);
		}
		sldMusic.addListener(new ChangeListener()
		{
			@Override
			public void changed( ChangeEvent event, Actor actor )
			{
				labelMusicPercents.setText(String.valueOf(MathUtils.round(sldMusic.getValue() * 100)) + "%");
			}
		});
		sldSound.addListener(new ChangeListener()
		{
			@Override
			public void changed( ChangeEvent event, Actor actor )
			{
				labelSoundPercent.setText(String.valueOf(MathUtils.round(sldSound.getValue() * 100)) + "%");
			}
		});
		// dodaj change listenery by ukrywaly oraz pokazywaly slidery w
		// zaleznosci od wcisnietego przycisku
		chkMusic.addListener(new ChangeListener()
		{
			@Override
			public void changed( ChangeEvent event, Actor actor )
			{
				if (GamePreferences.getInstance().sound)
				{
					MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
				}
				if (chkMusic.isChecked())
				{
					sldMusic.setVisible(true);
					labelMusicPercents.setVisible(true);
				}
				else
				{
					sldMusic.setVisible(false);
					labelMusicPercents.setVisible(false);
				}
			}
		});
		chkSound.addListener(new ChangeListener()
		{
			@Override
			public void changed( ChangeEvent event, Actor actor )
			{
				if (GamePreferences.getInstance().sound)
				{
					MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
				}
				if (chkSound.isChecked())
				{
					sldSound.setVisible(true);
					labelSoundPercent.setVisible(true);
				}
				else
				{
					sldSound.setVisible(false);
					labelSoundPercent.setVisible(false);
				}
			}
		});
		tableOptions.add(labelAskMusic).right();
		tableOptions.add(chkMusic).left().padLeft(10);
		tableOptions.add(sldMusic);
		tableOptions.add(labelMusicPercents).padLeft(10).width(100).expand().row();
		tableOptions.add(labelAskSound).right();
		tableOptions.add(chkSound).padLeft(10).left();
		tableOptions.add(sldSound);
		tableOptions.add(labelSoundPercent).padLeft(10).width(100).expand().row();
		tableOptions.add(labelSetCharacter).expandX().right();
		tableOptions.add(selectBoxCharacter).padRight(20);
		Pixmap pm1 = new Pixmap(1, 1, Format.RGB565);
		pm1.setColor(0f, 152 / 255f, 1f, 1f);
		pm1.fill();
		Table tablePlayerBackgroundImage = new Table();
		tablePlayerBackgroundImage.add(imagePlayerSelected).width(1.8f * imagePlayerSelected.getWidth()).height(2.0f * imagePlayerSelected.getHeight());
		tablePlayerBackgroundImage.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));
		tableOptions.add(tablePlayerBackgroundImage).expand().padTop(10);
		tableOptions.row();
		tableOptions.add(btnSave).padTop(10).expandX().colspan(2);
		tableOptions.add(btnBack).padTop(10).expandX().colspan(2);
		tableOptions.pack();
	
		selectBoxCharacter.addListener(new ChangeListener()
		{
			@Override
			public void changed( ChangeEvent event, Actor actor )
			{
				if (GamePreferences.getInstance().sound)
				{
					MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
				}
				gamerAtlasRegion = MyAssetsManager.instance.textures.get("assets1/Player/" + selectBoxCharacter.getSelected() + "Character");
				TextureRegion[][] temp = gamerAtlasRegion.split(gamerAtlasRegion.getRegionWidth() / Player.rowsInSpritesheet,
						gamerAtlasRegion.getRegionHeight() / Player.colsInSpritesheet);
				imagePlayerSelected.setDrawable(new TextureRegionDrawable(new TextureRegion(temp[8][0])));
			}
		});
		btnSave.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				if (GamePreferences.getInstance().sound)
				{
					MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
				}
				saveSettingsToFile();
				new Dialog("", skin, "dialog")
				{
					protected void result( Object object )
					{
						if (((Boolean) object).booleanValue())
						{
							if (GamePreferences.getInstance().sound)
							{
								MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
							}
							game.setScreen(new MenuScreen(game));
						}
					};
				}.text("Settings have been saved.").button("OK", true).show(stage).setSize(350, 150);
				tableOptions.setVisible(false);
			}
		});
		btnBack.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				if (GamePreferences.getInstance().sound)
				{
					MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
				}
				game.setScreen(new MenuScreen(game));
			}
		});
		rootTable.pack();
		stage.addActor(rootTable);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide()
	{
		saveSettingsToFile();
		// this.dispose();
	}

	@Override
	public void pause()
	{
	}

	private void loadSettingsFromFile()
	{
		GamePreferences prefs = GamePreferences.getInstance();
		prefs.load();
		chkSound.setChecked(prefs.sound);
		sldSound.setValue(prefs.volSound);
		chkMusic.setChecked(prefs.music);
		sldMusic.setValue(prefs.volMusic);
		selectBoxCharacter.setSelected(prefs.characterSkin);
	}

	private void saveSettingsToFile()
	{
		GamePreferences prefs = GamePreferences.getInstance();
		prefs.sound = chkSound.isChecked();
		prefs.volSound = sldSound.getValue();
		prefs.music = chkMusic.isChecked();
		prefs.volMusic = sldMusic.getValue();
		prefs.characterSkin = selectBoxCharacter.getSelected();
		prefs.savePreferences();
	}

	protected void onClickedSave()
	{
		this.saveSettingsToFile();
		game.setScreen(new MenuScreen(game));
	}
}
