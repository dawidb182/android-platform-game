
package pl.majority.game.screens;

import pl.majority.game.objects.AnimatedImageOnScene2DUI;
import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.objects.MyOwnDialogLibgdx;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FillViewport;

public class MenuScreen extends AbstractScreen
{
	private Stage stage;
	// LOGO STRONA GLOWNA
	private TextureRegion backgroundImage;
	private Image imgBackground;
	private AnimatedImageOnScene2DUI animation;
	private Table rootTable, buttonsTable;
	public Skin skin;
	private Label labelGameTitle;
	private TextButton buttonPlay, buttonExit, buttonEditor, buttonOptions;
	private Window window;
	private SpriteBatch sb;

	public MenuScreen( Game game )
	{
		super(game);
		skin = MyAssetsManager.instance.skin;
		backgroundImage = MyAssetsManager.instance.textures.get("backgroundImage");
		stage = new Stage(new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		sb = new SpriteBatch();
		rootTable = new Table();
		buttonsTable = new Table();
		animation = new AnimatedImageOnScene2DUI();
		labelGameTitle = new Label("Majority", skin, "labelGameTitle");
		labelGameTitle.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				window.setVisible(true);
			}
		});
		imgBackground = new Image(MyAssetsManager.instance.textures.get("assets1/Player/clothCharacter").getTexture());
		imgBackground.setScaling(Scaling.fit);
		TextureRegionDrawable temp = new TextureRegionDrawable(MyAssetsManager.instance.textures.get("mainButton"));
		buttonPlay = new TextButton("Play", skin, "mainSetting");
		buttonExit = new TextButton("Exit", skin, "mainSetting");
		buttonEditor = new TextButton("Editor", skin, "mainSetting");
		buttonOptions = new TextButton("Options", skin, "mainSetting");
		buttonPlay.setBackground(temp);
		buttonExit.setBackground(temp);
		buttonEditor.setBackground(temp);
		buttonOptions.setBackground(temp);
	}

	@Override
	public void render( float deltaTime )
	{
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		animation.act(deltaTime);
		sb.begin();
		sb.draw(backgroundImage, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		animation.draw(sb, 1.0f);
		sb.end();
		stage.act(deltaTime);
		stage.draw();
	}

	@Override
	public void resize( int width, int height )
	{
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show()
	{
		if (GamePreferences.getInstance().drawDebug)
		{
			this.rootTable.debug();
			this.buttonsTable.debug();
		}
		// move all internal maps to one folder with user created maps
		try
		{
			Gdx.files.internal("levels/").copyTo(Gdx.files.local("./bin/levels/"));
		}
		catch (GdxRuntimeException e)
		{
			Gdx.app.log("NO MAPS", "THERE ARE NO INTERNAL MAPS");
		}
		addClickListenersToButtons();
		buildMainMenuButtons();
		buildRootTable();
		prepareAuthorWindow();
		stage.addActor(rootTable);
		Gdx.input.setInputProcessor(stage);
		if (GamePreferences.getInstance().music)
		{
			MyAssetsManager.instance.mainScreenMusic.setVolume(GamePreferences.getInstance().volMusic);
			MyAssetsManager.instance.mainScreenMusic.play();
		}
		else
		{
			MyAssetsManager.instance.mainScreenMusic.pause();
		}
	}

	private void addClickListenersToButtons()
	{
		buttonPlay.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				game.setScreen(new LevelSelectScreen(game));
			}
		});
		buttonEditor.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				game.setScreen(new EditorScreen(game));
			}
		});
		buttonOptions.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				game.setScreen(new SettingsScreen(game));
			}
		});
		buttonExit.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				new MyOwnDialogLibgdx("Confirm")
				{
					protected void result( Object object )
					{
						MyAssetsManager.instance.playClickSound();
						if (((Boolean) object).booleanValue())
						{
							Gdx.app.exit();
						}
					}
				}.text("Are you sure?").button("Yes", true).button("No", false).key(Keys.ENTER, true).key(Keys.ESCAPE, false).show(stage);
			}
		});
	}

	@Override
	public void hide()
	{
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void dispose()
	{
		stage.dispose();
		sb.dispose();
	}

private void prepareAuthorWindow()
{
	window = new Window("", skin, "default");
	window.setVisible(false);
	TextButton xToCloseButton = new TextButton("X", skin);
	xToCloseButton.addListener(new ClickListener()
	{
		@Override
		public void clicked( InputEvent event, float x, float y )
		{
			window.setVisible(false);
		}
	});
	window.getButtonTable().add(xToCloseButton).height(window.getPadTop());//.height(20).width(20);
	window.add(new Image(MyAssetsManager.instance.textures.get("logopb"))).padTop(120).row();
	LabelStyle styleAuthor = new LabelStyle();
	styleAuthor.font = MyAssetsManager.instance.fontNormal;
	styleAuthor.fontColor = Color.BLACK;
	window.add(
			new Label(
					"Niniejsza gra zostala zrealizowana jako praca\ndyplomowa inzynierska na Wydziale Informatyki\nPolitechniki Bialostockiej.\nAutor: Dawid Banasik.",
					styleAuthor)).pad(20);
	window.setPosition(10, 10);
	window.pack();
	stage.addActor(window);
}
	private void buildRootTable()
	{
		rootTable.add(labelGameTitle).padTop(20).padBottom(20).colspan(2).expandX().row();
		rootTable.add(animation).pad(3).expand();
		rootTable.add(buttonsTable).padRight(50).left();
		rootTable.setFillParent(true);
	}

	private void buildMainMenuButtons()
	{
		buttonsTable.add(buttonOptions).size(160, 160).pad(10).right().bottom();
		buttonsTable.add(buttonPlay).size(120, 120).pad(10).left().bottom().row();
		buttonsTable.add(buttonEditor).size(90, 90).pad(10).right().top();
		buttonsTable.add(buttonExit).size(150, 150).pad(10).left().top();
	}
}
