
package pl.majority.game.screens;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;

import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.objects.EditorTableCell;
import pl.majority.game.objects.MyOwnDialogLibgdx;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.badlogic.gdx.utils.XmlWriter;
import com.badlogic.gdx.utils.viewport.FillViewport;

public class EditorScreen extends AbstractScreen
{
	private final int mapHeight = 20;
	private final int mapWidth = 50;
	private Stage stage;
	private Skin skin;
	private ImageButton btnSave, btnBack, btnDelete, btnOpen;
	private CheckBox checkBoxCloudsVisible, checkboxLinesVisible;
	private String mapPath;
	private SpriteBatch sb;
	private TextureRegion backgroundImage;
	private SelectBox<String> selectBoxMap;
	private ScrollPane scrollPaneMainBoard, scrollPaneAssets;
	private Table tableScrollMainBoard, rootTable;
	private VerticalGroup verticalGroupAssets;
	private ScrollPane scrollPaneMapNames;
	private SelectBox<String> selectBoxAssetType;
	private EditorTableCell[][] imageCellMainBoard;
	public Map<String, AtlasRegion> textures;
	private Table leftSideOfScreen;
	private Label labelGametitle;
	private Array<ImageButton> tiles, enemies, extras, bonuses, points, shelfs;
	private Array<Array<Vector2>> shelfTrace;
	private Array<Vector2> tempTrace;
	private Image selectedTile;
	private Pixmap pixMapBlue;
	
	private Vector2 initialPosition;

	public EditorScreen( Game game )
	{
		super(game);
		mapPath = "";
		skin = MyAssetsManager.instance.skin;
		// sr = new ShapeRenderer();
		selectBoxMap = new SelectBox<String>(skin, "selectBoxEditor");
		shelfTrace = new Array<Array<Vector2>>();
		tempTrace = new Array<Vector2>();
		selectedTile = null;
		textures = MyAssetsManager.instance.textures;
		sb = new SpriteBatch();
		stage = new Stage(new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		tiles = new Array<ImageButton>();
		enemies = new Array<ImageButton>();
		extras = new Array<ImageButton>();
		bonuses = new Array<ImageButton>();
		points = new Array<ImageButton>();
		shelfs = new Array<ImageButton>();
		initialPosition = new Vector2(0,0);
		for (Entry<String, AtlasRegion> entry : textures.entrySet())
		{
			String name = entry.getKey();
			if (name.contains("assets1"))
			{
				if (name.contains("Items"))
				{
					if (!name.contains("cloud"))
					{
						ImageButton ib = prepareButton(entry.getValue(), name);
						extras.add(ib);
					}
				}
				else
					if (name.contains("Tiles"))
					{
						ImageButton ib = prepareButton(entry.getValue(), name);
						tiles.add(ib);
					}
					else
						if (name.contains("Platforms"))
						{
							ImageButton ib = prepareButton(entry.getValue(), name);
							shelfs.add(ib);
						}
			}
		}
		// add points
		points.add(prepareButton(textures.get("assets1/Other/signExit"), "assets1/Other/signExit"));
		points.add(prepareButton(textures.get("assets1/Other/signStart"), "assets1/Other/signStart"));
		// add bonuses///////////////////////////
		bonuses.add(prepareButton(textures.get("assets1/Other/coinGold"), "assets1/Other/coinGold"));
		bonuses.add(prepareButton(textures.get("assets1/Other/coinBronze"), "assets1/Other/coinBronze"));
		bonuses.add(prepareButton(textures.get("assets1/Other/coinSilver"), "assets1/Other/coinSilver"));
		// add enemies///////////////////////////
		enemies.add(prepareButton(textures.get("assets1/Enemies/blockerMad"), "assets1/Enemies/blockerMad"));
		enemies.add(prepareButton(textures.get("assets1/Enemies/fishSwim1"), "assets1/Enemies/fishSwim1"));
		enemies.add(prepareButton(textures.get("assets1/Enemies/flyFly1"), "assets1/Enemies/flyFly1"));
		enemies.add(prepareButton(textures.get("assets1/Enemies/slimeWalk1"), "assets1/Enemies/slimeWalk1"));
		enemies.add(prepareButton(textures.get("assets1/Enemies/snailWalk1"), "assets1/Enemies/snailWalk1"));
		// ////////////////////////////////////////
		// root table which coordinates foundamential of design
		rootTable = new Table();
		leftSideOfScreen = new Table();
		btnOpen = new ImageButton(new TextureRegionDrawable(textures.get("shadedDark36")));
		Label labelBtnOpen = new Label("Open", skin);
		labelBtnOpen.setPosition(btnOpen.getX() - 8, btnOpen.getY() - btnOpen.getHeight() + 5);
		btnOpen.addActor(labelBtnOpen);
		btnSave = new ImageButton(new TextureRegionDrawable(textures.get("shadedDark34")));
		Label labelBtnSave = new Label("Save", skin);
		labelBtnSave.setPosition(btnSave.getX() - 2, btnSave.getY() - btnSave.getHeight() + 5);
		btnSave.addActor(labelBtnSave);
		btnBack = new ImageButton(new TextureRegionDrawable(textures.get("shadedDark21")));
		Label labelBtnBack = new Label("Back", skin);
		labelBtnBack.setPosition(btnBack.getX() - 6, btnBack.getY() - btnBack.getHeight() + 5);
		btnBack.addActor(labelBtnBack);
		btnDelete = new ImageButton(new TextureRegionDrawable(textures.get("shadedDark35")));
		Label labelBtnDelete = new Label("Delete", skin);
		labelBtnDelete.setPosition(btnDelete.getX() - 15, btnDelete.getY() - btnDelete.getHeight() + 5);
		btnDelete.addActor(labelBtnDelete);
		checkBoxCloudsVisible = new CheckBox("Clouds?", skin);
		checkboxLinesVisible = new CheckBox("Lines?", skin);
		checkboxLinesVisible.setChecked(true);
		selectBoxAssetType = new SelectBox<String>(skin, "selectBoxEditor");
		selectBoxAssetType.setItems("TILE", "EXTRA", "SHELF", "ENEMY", "BONUS", "POINT");
		selectBoxAssetType.setSelected("TILE");
		labelGametitle = new Label("Map editor", skin, "default");
		// main movealbe board table
		// set background the same like in real game
		pixMapBlue = new Pixmap(70, 70, Format.RGB565);
		pixMapBlue.setColor(0f, 152 / 255f, 1f, 1f);
		pixMapBlue.fill();
		tableScrollMainBoard = new Table();
		// vertical group of tile
		verticalGroupAssets = new VerticalGroup();
		verticalGroupAssets.pad(10);
		// vertical group of user created map
		scrollPaneMapNames = new ScrollPane(selectBoxAssetType, skin, "scrollPaneAssets");
		scrollPaneMapNames.setScrollingDisabled(true, false);
		// makes it scroll
		scrollPaneMainBoard = new ScrollPane(tableScrollMainBoard, skin, "scrollPaneMap");
		scrollPaneAssets = new ScrollPane(verticalGroupAssets, skin, "scrollPaneAssets");
		scrollPaneAssets.setScrollingDisabled(true, false);
		backgroundImage = textures.get("backgroundImage");
		imageCellMainBoard = new EditorTableCell[mapHeight][mapWidth]; // container
																		// for
		// images && texture
		prepareInitialMap(imageCellMainBoard);
		if (checkboxLinesVisible.isChecked())
		{
			tableScrollMainBoard.setDebug(true);
		}
		if (GamePreferences.getInstance().drawDebug)
		{
			tableScrollMainBoard.setDebug(true);
			verticalGroupAssets.setDebug(true);
			leftSideOfScreen.setDebug(true);
			rootTable.setDebug(true);
			btnSave.setDebug(true);
			btnOpen.setDebug(true);
			btnBack.setDebug(true);
			btnDelete.setDebug(true);
			checkBoxCloudsVisible.setDebug(true);
			checkboxLinesVisible.setDebug(true);
		}
	}

	private ImageButton prepareButton( AtlasRegion buttonValue, String name )
	{
		ImageButton ib = new ImageButton(new TextureRegionDrawable(buttonValue));
		ib.setName(name);
		ib.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				selectedTile = new Image(textures.get(event.getListenerActor().getName()));
				selectedTile.setName(event.getListenerActor().getName());
			}
		});
		return ib;
	}

	@Override
	public void render( float deltaTime )
	{
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		sb.begin();
		sb.draw(backgroundImage, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		sb.end();
		stage.act(deltaTime);
		stage.draw();
	}

	@Override
	public void resize( int width, int height )
	{
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show()
	{
		Gdx.input.setInputProcessor(stage);
		checkBoxCloudsVisible.addListener(new ClickListener()
		{
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
			};
		});
		checkboxLinesVisible.addListener(new ClickListener()
		{
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				tableScrollMainBoard.setDebug(checkboxLinesVisible.isChecked());
			};
		});
		selectBoxMap.addListener(new ChangeListener()
		{
			@Override
			public void changed( ChangeEvent event, Actor actor )
			{
				MyAssetsManager.instance.playClickSound();
				mapPath = selectBoxMap.getSelected();
				loadMapFromXMLFile(mapPath);
				scrollPaneMainBoard.scrollTo(initialPosition.x * 70, initialPosition.y * 70, 70, 70,true,true);
			}
		});
		selectBoxMap.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				// update map list
				Array<String> mapNames = new Array<String>();
				FileHandle dirHandle = Gdx.files.local("./bin/levels/");
				for (FileHandle entry : dirHandle.list())
				{
					if (entry.file().getName().toUpperCase().contains("XML"))
					{
						mapNames.add(entry.file().getName());
					}
				}
				selectBoxMap.setItems(mapNames);
				MyAssetsManager.instance.playClickSound();
			}
		});
		btnBack.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				new MyOwnDialogLibgdx("Confirm")
				{
					protected void result( Object object )
					{
						MyAssetsManager.instance.playClickSound();
						if (((Boolean) object).booleanValue())
						{
							dispose();
							game.setScreen(new MenuScreen(game));
						}
					};
				}.text("Are you sure?").button("YES", true).button("NO", false).show(stage)
						.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Align.center);
			}
		});
		selectBoxAssetType.addListener(new ChangeListener()
		{
			@Override
			public void changed( ChangeEvent event, Actor actor )
			{
				MyAssetsManager.instance.playClickSound();
				String selectedAssetsType = selectBoxAssetType.getSelected().toUpperCase();
				verticalGroupAssets.clear();
				verticalGroupAssets.addActor(selectBoxAssetType);
				verticalGroupAssets.space(5);
				switch (selectedAssetsType)
				{
					case "TILE" :
						for (ImageButton ib : tiles)
						{
							verticalGroupAssets.addActor(ib);
						}
						break;
					case "EXTRA" :
						for (ImageButton ib : extras)
						{
							Actor a = ib;
							a.setName(ib.getName());
							verticalGroupAssets.addActor(a);
						}
						break;
					case "SHELF" :
						for (ImageButton ib : shelfs)
						{
							Actor a = ib;
							a.setName(ib.getName());
							verticalGroupAssets.addActor(a);
						}
						break;
					case "ENEMY" :
						for (ImageButton ib : enemies)
						{
							verticalGroupAssets.addActor(ib);
						}
						break;
					case "BONUS" :
						for (ImageButton ib : bonuses)
						{
							verticalGroupAssets.addActor(ib);
						}
						break;
					case "POINT" :
						for (ImageButton ib : points)
						{
							verticalGroupAssets.addActor(ib);
						}
						break;
				}
			}
		});
		selectBoxAssetType.addListener(new ClickListener()
		{
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
			}
		});
		btnSave.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				if (isBeginPointSet() && isEndPointSet())
				{
					String filenameCreated = "";
					int number = 0;
					FileHandle dirHandle = Gdx.files.local("./bin/levels/");
					for (FileHandle entry : dirHandle.list())
					{
						// for every file in levels folder which contains "map"
						// text
						// to create initial map filename
						if (entry.file().getName().contains("map"))
						{
							number++;
						}
					}
					filenameCreated += "map";
					filenameCreated += String.valueOf(number);
					// IN CASE OF JAVA SWING CRASHING!!!!! UNCOMMENT UNDER IT TOO
					// if (Gdx.app.getType() == ApplicationType.Android)
					// {
					Gdx.input.getTextInput(new TextInputListener()
					{
						@Override
						public void input( String text )
						{
							if (text.length() == 0)
							{
								MyOwnDialogLibgdx dialog = new MyOwnDialogLibgdx("Error")
								{
									protected void result( Object object )
									{
										if (((Boolean) object).booleanValue())
										{
											MyAssetsManager.instance.playClickSound();
										}
									}
								};
								dialog.text("Enter name.").button("OK", true);
								dialog.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Align.center);
								dialog.show(stage);
							}
							else
								if (text.length() > 10)
								{
									MyOwnDialogLibgdx dialog = new MyOwnDialogLibgdx("Too long")
									{
										protected void result( Object object )
										{
											if (((Boolean) object).booleanValue())
											{
												MyAssetsManager.instance.playClickSound();
											}
										}
									};
									dialog.text("Max 10 characters.").button("OK", true);
									dialog.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Align.center);
									dialog.show(stage);
								}
								else
								{
									try
									{
										saveMapToXML(text += ".xml");
										MyOwnDialogLibgdx dialog = new MyOwnDialogLibgdx("")
										{
											protected void result( Object object )
											{
												if (((Boolean) object).booleanValue())
												{
													MyAssetsManager.instance.playClickSound();
													dispose();
													game.setScreen(new MenuScreen(game));
												}
											}
										};
										dialog.text("Map have been saved.").button("OK", true);
										dialog.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Align.center);
										dialog.show(stage);
									}
									catch (IOException | NumberFormatException e)
									{
										Gdx.app.error("XML SAVING ERROR", "WE HAVE A PROBLEM");
										e.printStackTrace();
									}
								}
						}

						@Override
						public void canceled()
						{
						}
					}, "Enter map name:", filenameCreated, "");
					// }
					// else
					// if (Gdx.app.getType() == ApplicationType.Desktop)
					// {
					// try
					// {
					// saveMapToXML(filenameCreated += ".xml");
					// new MyOwnDialogLibgdx("")
					// {
					// protected void result( Object object )
					// {
					// if (((Boolean) object).booleanValue())
					// {
					// Assets.playClickSound();
					// game.setScreen(new MenuScreen(game));
					// }
					// };
					// }.text("Map have been saved.").button("OK", true).show(stage)
					// .setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Align.center);
					// }
					// catch (IOException e)
					// {
					// Gdx.app.error("XML SAVING ERROR", "WE HAVE A PROBLEM");
					// e.printStackTrace();
					// }
					// }
				}
				else
				{
					new MyOwnDialogLibgdx("Error")
					{
						protected void result( Object object )
						{
							MyAssetsManager.instance.playClickSound();
						};
					}.text("Set begin and end!").button("OK", true).show(stage)
							.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Align.center);
				}
			}
		});
		btnDelete.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				selectedTile = null;
			}
		});
		btnOpen.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MyAssetsManager.instance.playClickSound();
				Array<String> mapNames = new Array<String>();
				FileHandle dirHandlePlayerMaps;
				dirHandlePlayerMaps = Gdx.files.local("./bin/levels/");
				for (FileHandle entry : dirHandlePlayerMaps.list())
				{
					if (entry.file().getName().toUpperCase().contains("XML"))
					{
						mapNames.add(entry.file().getName());
					}
				}
				selectBoxMap.setItems(mapNames);
				selectBoxMap.setWidth(80);
				if (mapNames.size > 0)
				{
					leftSideOfScreen.getCell(btnOpen).setActorWidth(80);
					leftSideOfScreen.getCell(btnOpen).setActor(selectBoxMap).center();
				}
				else
				{
					MyOwnDialogLibgdx ownDialog = new MyOwnDialogLibgdx("");
					ownDialog.text("There aren't any maps").button("OK", true).show(stage)
							.setPosition(Gdx.graphics.getWidth() / 2 - 400, Gdx.graphics.getHeight() / 2 - 200);
					ownDialog.setSize(400, 200);
				}
			}
		});
		// create empty scroll pane
		for (int i = 0; i < mapHeight; i++)
		{
			for (int j = 0; j < mapWidth; j++)
			{
				tableScrollMainBoard.add(imageCellMainBoard[i][j]).width(70).height(70);
				// fill blue color
			}
			tableScrollMainBoard.row();
		}
		tableScrollMainBoard.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pixMapBlue))));
		leftSideOfScreen.add(scrollPaneMainBoard).colspan(7).row();
		leftSideOfScreen.add(btnOpen).top().setActorWidth(80);
		leftSideOfScreen.add(btnSave).top().setActorWidth(80);
		leftSideOfScreen.add(btnBack).top().setActorWidth(80);
		leftSideOfScreen.add(btnDelete).top().setActorWidth(80);
		leftSideOfScreen.add(checkBoxCloudsVisible).width(140).height(80);
		leftSideOfScreen.add(checkboxLinesVisible).width(130).height(80);
		leftSideOfScreen.layout();
		scrollPaneMainBoard.scrollTo(0, 0, 70, 70,true,true); // moves to begin of created
													// map
		scrollPaneMainBoard.setupOverscroll(15, 25, 50);
		rootTable.setFillParent(true);
		rootTable.add(labelGametitle).colspan(2).align(Align.center).row();
		rootTable.add(leftSideOfScreen);
		rootTable.add(scrollPaneAssets).width(100).align(Align.top);
		rootTable.center();
		// fill with tiles
		verticalGroupAssets.addActor(scrollPaneMapNames);
		verticalGroupAssets.space(5);
		switch (selectBoxAssetType.getSelected().toUpperCase())
		{
			case "TILE" :
				for (ImageButton ib : tiles)
				{
					verticalGroupAssets.addActor(ib);
				}
				break;
			case "EXTRA" :
				for (ImageButton ib : extras)
				{
					verticalGroupAssets.addActor(ib);
				}
				break;
			case "ENEMY" :
				for (ImageButton ib : enemies)
				{
					verticalGroupAssets.addActor(ib);
				}
				break;
			case "BONUS" :
				for (ImageButton ib : bonuses)
				{
					verticalGroupAssets.addActor(ib);
				}
				break;
			case "POINT" :
				for (ImageButton ib : points)
				{
					verticalGroupAssets.addActor(ib);
				}
			case "SHELF" :
				for (ImageButton ib : shelfs)
				{
					verticalGroupAssets.addActor(ib);
				}
				break;
		}
		this.stage.addActor(rootTable);
	}

	@Override
	public void hide()
	{
	}

	@Override
	public void pause()
	{
	}

	public void saveMapToXML( String filename ) throws IOException
	{
		// .element - new <>
		// .attribute - new <asdasd --->attr=""<---- >
		// .pop - new </end>
		StringWriter writer = new StringWriter();
		XmlWriter xml = new XmlWriter(writer);
		Array<String> enemies, items, bonuses, tiles, jumpingPlatforms;
		enemies = new Array<String>();
		items = new Array<String>();
		bonuses = new Array<String>();
		tiles = new Array<String>();
		jumpingPlatforms = new Array<String>();
		String finalPoint = "";
		String player = ""; // ma punkt startowy
		for (int i = 0; i < mapHeight; i++)
		{
			for (int j = 0; j < mapWidth; j++)
			{ // pattern: posX-posY-name
				String tileName = imageCellMainBoard[i][j].getName();
				String[] splitted = tileName.split("-");
				int xPosition = Integer.valueOf(splitted[1]);
				int yPosition = Integer.valueOf(splitted[0]);
				if (imageCellMainBoard[i][j].additionals.size != 0 && !imageCellMainBoard[i][j].isPlatformAdditional())
				{
					for (int k = 0; k < imageCellMainBoard[i][j].additionals.size; k++)
					{
						String[] additionalNameSplitted = imageCellMainBoard[i][j].additionals.get(k).getName().split("-");
						String additionalName = additionalNameSplitted[2];
						items.add(xPosition + "-" + yPosition + "-" + additionalName);
					}
				}
				// foreach putted tiles
				if (!splitted[2].equals("empty"))
				{
					String[] splittedSplit = splitted[2].split("/");
					switch (splittedSplit[1].toUpperCase())
					{
						case "ENEMIES" :
							enemies.add(xPosition + "-" + yPosition + "-" + splitted[2]);
							break;
						case "ITEMS" :
							items.add(xPosition + "-" + yPosition + "-" + splitted[2]);
							break;
						case "OTHER" :
							String otherName = splittedSplit[2].toUpperCase();
							if (otherName.contains("COIN"))
							{
								bonuses.add(xPosition + "-" + yPosition + "-" + splitted[2]);
							}
							else
								if (otherName.contains("SIGNEXIT"))
								{
									finalPoint = xPosition + "-" + yPosition + "-" + splitted[2];
								}
								else
									if (otherName.contains("SIGNSTART"))
									{
										player = xPosition + "-" + yPosition + "-" + splitted[2];
									}
							break;
						case "TILES" :
							tiles.add(xPosition + "-" + yPosition + "-" + splitted[2]);
							break;
						case "PLATFORMS" :
							if (splittedSplit[2].contains("board"))
							{
								jumpingPlatforms.add(xPosition + "-" + yPosition + "-" + splitted[2]);
							}
							break;
					}
				}
			}
		}
		xml.element("map");
		xml.element("tiles");
		for (String s : tiles)
		{
			String[] splitted = s.split("-");
			String name = splitted[2];
			int posX = Integer.valueOf(splitted[0]);
			int posY = Integer.valueOf(splitted[1]);
			xml.element("tile").attribute("name", name).attribute("posX", posX).attribute("posY", posY).pop();
		}
		xml.pop();
		xml.element("additionals");
		for (String s : items)
		{
			String[] splitted = s.split("-");
			String name = splitted[2].toUpperCase();
			// for flags
			if (name.contains("FLAGYELLOW"))// FLAGYELLOW, FLAGRED, FLAGGREEN,
											// FLAGBLUE
			{
				name = "FLAGYELLOW";
			}
			else
				if (name.contains("FLAGRED"))
				{
					name = "FLAGRED";
				}
				else
					if (name.contains("FLAGGREEN"))
					{
						name = "FLAGGREEN";
					}
					else
						if (name.contains("FLAGBLUE"))
						{
							name = "FLAGBLUE";
						}
						else
							if (name.contains("TORCH"))
							{
								name = "TORCH";
							}
			int posX = Integer.valueOf(splitted[0]);
			int posY = Integer.valueOf(splitted[1]);
			xml.element("additional").attribute("name", name).attribute("posX", posX).attribute("posY", posY).pop();
		}
		xml.pop();
		xml.element("bonuses");
		for (String s : bonuses)
		{
			String[] splitted = s.split("-");
			String name = splitted[2];
			// for flags
			if (name.contains("coinGold"))// coinGold, coinBronze, coinSilver,
			{
				name = "coinGold";
			}
			else
				if (name.contains("coinBronze"))
				{
					name = "coinBronze";
				}
				else
					if (name.contains("coinSilver"))
					{
						name = "coinSilver";
					}
			int posX = Integer.valueOf(splitted[0]);
			int posY = Integer.valueOf(splitted[1]);
			xml.element("bonus").attribute("name", name).attribute("posX", posX).attribute("posY", posY);
			xml.pop();
		}
		xml.pop();
		xml.element("enemies");
		for (String s : enemies)
		{
			String[] splitted = s.split("-");
			String name = splitted[2].toUpperCase();
			// BLOCKER, FISH, FLY, SLIME, SNAIL
			if (name.contains("BLOCKER"))
			{
				name = "BLOCKER";
			}
			else
				if (name.contains("FISH"))
				{
					name = "FISH";
				}
				else
					if (name.contains("FLY"))
					{
						name = "FLY";
					}
					else
						if (name.contains("SLIME"))
						{
							name = "SLIME";
						}
						else
							if (name.contains("SNAIL"))
							{
								name = "SNAIL";
							}
			int posX = Integer.valueOf(splitted[0]);
			int posY = Integer.valueOf(splitted[1]);
			xml.element("enemy").attribute("name", name).attribute("posX", posX).attribute("posY", posY);
			xml.pop();
		}
		xml.pop();
		// moving platforms
		xml.element("platforms");
		if (shelfTrace.size > 0)
		{
			for (Array<Vector2> trace : shelfTrace)
			{
				xml.element("platform").attribute("name",
						imageCellMainBoard[this.mapHeight - 1 - (int) trace.get(0).y][(int) trace.get(0).x].getName().split("-")[2]);
				for (Vector2 tracePoint : trace)
				{
					xml.element("point").attribute("x", (int) tracePoint.x).attribute("y", (int) tracePoint.y).pop();
				}
				xml.pop();
			}
		}
		xml.pop();
		// jumping platforms
		xml.element("jumpingPlatforms");
		for (String s : jumpingPlatforms)
		{
			String[] splitted = s.split("-");
			String name = splitted[2];
			int posX = Integer.valueOf(splitted[0]);
			int posY = Integer.valueOf(splitted[1]);
			xml.element("platform").attribute("name", name).attribute("posX", posX).attribute("posY", posY).pop();
		}
		xml.pop();
		// clouds
		if (checkBoxCloudsVisible.isChecked())
		{
			xml.element("clouds").attribute("visible", "true").pop();
		}
		else
		{
			xml.element("clouds").attribute("visible", "false").pop();
		}
		// PLAYER
		String[] splittedplayer = player.split("-");
		int posX = Integer.valueOf(splittedplayer[0]);
		int posY = Integer.valueOf(splittedplayer[1]);
		xml.element("player").attribute("posX", posX).attribute("posY", posY).pop();
		// final point
		String[] splittedfinalPoint = finalPoint.split("-");
		posX = Integer.valueOf(splittedfinalPoint[0]);
		posY = Integer.valueOf(splittedfinalPoint[1]);
		xml.element("final").attribute("posX", posX).attribute("posY", posY).pop();
		xml.pop();
		xml.close();
		// finally save created XML
		FileHandle tempFileHandle;
		if (Gdx.app.getType() == ApplicationType.Android)
		{
			tempFileHandle = Gdx.files.local("./bin/levels/" + filename);
		}
		else
		{
			tempFileHandle = Gdx.files.local("levels/" + filename);
		}
		tempFileHandle.writeString(writer.toString(), false);
		//System.out.println(writer.toString());
	}

	private void prepareInitialMap( final EditorTableCell[][] imageCellMainBoard )
	{
		// create images
		for (int i = 0; i < mapHeight; i++) // height
		{
			for (int j = 0; j < mapWidth; j++) // width
			{
				int yOnMap = mapHeight - 1 - i;
				imageCellMainBoard[yOnMap][j] = new EditorTableCell(); // textures.get("empty")
				imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + "empty");
				imageCellMainBoard[yOnMap][j].addListener(new ClickListener()
				{
					@Override
					public void clicked( InputEvent event, float x, float y )
					{
						MyAssetsManager.instance.playClickSound();
						String[] nameSplittedOfOnBoardTile = event.getListenerActor().getName().split("-");
						int i = Integer.valueOf(nameSplittedOfOnBoardTile[0]);
						int j = Integer.valueOf(nameSplittedOfOnBoardTile[1]);
						if (selectedTile != null)
						{
							String nameOfSelectedTile = selectedTile.getName();
							String clickedTileOnBoard = nameSplittedOfOnBoardTile[2];
							// to avoid multiple adding start point
							if (nameOfSelectedTile.toUpperCase().contains("SIGNSTART") && isBeginPointSet())
							{
								return;
							}
							// to avoid multiple adding end point
							if (nameOfSelectedTile.toUpperCase().contains("SIGNEXIT") && isEndPointSet())
							{
								return;
							}
							// to avoid adding moving platform in tiles
							if (nameOfSelectedTile.toUpperCase().contains("PLATFORMS")
									&& (!clickedTileOnBoard.toUpperCase().contains("EMPTY") && !clickedTileOnBoard.toUpperCase().contains("PLATFORMS")))
							{
								return;
							}
							if (nameOfSelectedTile.toUpperCase().contains("PLATFORMS"))
							{
								if (nameOfSelectedTile.toUpperCase().contains("BOARD"))
								{
									int yOnMap = mapHeight - 1 - i;
									imageCellMainBoard[yOnMap][j].setDrawable(new TextureRegionDrawable(textures.get(nameOfSelectedTile)));
									imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + selectedTile.getName());
								}
								else
								{
									if (tempTrace.size > 0)
									{
										Vector2 beginPoint = tempTrace.get(0);
										if (j == (int) beginPoint.x && i == (int) beginPoint.y && tempTrace.size > 1)
										{
											scrollPaneAssets.setVisible(true);
											btnDelete.setVisible(true);
											// finish creating trace
											shelfTrace.add(new Array<Vector2>(tempTrace));
											tempTrace.clear();
										}
										else
										{
											int yOnMap = mapHeight - 1 - i;
											tempTrace.add(new Vector2(j, i));
											// add number as additional
											String number = String.valueOf(tempTrace.size / 10);
											Image additional = new Image(textures.get("assets1/Symbols/SYMB" + number));
											additional.setName(i + "-" + j + "-" + nameOfSelectedTile);
											additional.setWidth(event.getListenerActor().getWidth() / 2);
											additional.setHeight(event.getListenerActor().getHeight() / 2);
											additional.setPosition(event.getListenerActor().getX(), event.getListenerActor().getY());
											imageCellMainBoard[yOnMap][j].setPlatformAdditional(true);
											imageCellMainBoard[yOnMap][j].additionals.add(additional);
											number = String.valueOf(tempTrace.size % 10);
											additional = new Image(textures.get("assets1/Symbols/SYMB" + number));
											additional.setName(i + "-" + j + "-" + nameOfSelectedTile);
											additional.setWidth(event.getListenerActor().getWidth() / 2);
											additional.setHeight(event.getListenerActor().getHeight() / 2);
											additional.setPosition(event.getListenerActor().getX() + (event.getListenerActor().getWidth() / 2), event
													.getListenerActor().getY());
											imageCellMainBoard[yOnMap][j].additionals.add(additional);
											imageCellMainBoard[yOnMap][j].setDrawable(new TextureRegionDrawable(textures.get(nameOfSelectedTile)));
											imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + selectedTile.getName());
										}
									}
									else
									{
										int yOnMap = mapHeight - 1 - i;
										scrollPaneAssets.setVisible(false);
										btnDelete.setVisible(false);
										tempTrace.add(new Vector2(j, i));
										// add first point
										Image additional = new Image(textures.get(searchFullPathInMap("SYMB0")));
										additional.setName(i + "-" + j + "-" + "assets1/Symbols/SYMB0");
										additional.setWidth(event.getListenerActor().getWidth() / 2);
										additional.setHeight(event.getListenerActor().getHeight() / 2);
										additional.setPosition(event.getListenerActor().getX(), event.getListenerActor().getY());
										imageCellMainBoard[yOnMap][j].setPlatformAdditional(true);
										imageCellMainBoard[yOnMap][j].additionals.add(additional);
										additional = new Image(textures.get("assets1/Symbols/SYMB1"));
										additional.setName(i + "-" + j + "-" + "assets1/Symbols/SYMB1");
										additional.setWidth(event.getListenerActor().getWidth() / 2);
										additional.setHeight(event.getListenerActor().getHeight() / 2);
										additional.setPosition(event.getListenerActor().getX() + (event.getListenerActor().getWidth() / 2), event
												.getListenerActor().getY());
										imageCellMainBoard[yOnMap][j].additionals.add(additional);
										imageCellMainBoard[yOnMap][j].setDrawable(new TextureRegionDrawable(textures.get(nameOfSelectedTile)));
										imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + selectedTile.getName());
									}
								}
							}
							else
							{
								if (clickedTileOnBoard.toUpperCase().contains("EMPTY"))
								{
									int yOnMap = mapHeight - 1 - i;
									imageCellMainBoard[yOnMap][j].setDrawable(new TextureRegionDrawable(textures.get(nameOfSelectedTile)));
									imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + selectedTile.getName());
								}
								if (clickedTileOnBoard.toUpperCase().contains("TILES"))
								{
									if (nameOfSelectedTile.toUpperCase().contains("ITEMS") || nameOfSelectedTile.toUpperCase().contains("OTHER"))
									{
										Image additional = new Image(textures.get(nameOfSelectedTile));
										additional.setName(i + "-" + j + "-" + nameOfSelectedTile);
										additional.setWidth(event.getListenerActor().getWidth());
										additional.setHeight(event.getListenerActor().getHeight());
										additional.setPosition(event.getListenerActor().getX(), event.getListenerActor().getY());
										imageCellMainBoard[mapHeight - 1 - i][j].additionals.add(additional);
									}
								}
							}
						}
						else
						// deleting
						{
							String[] nameSplitted = imageCellMainBoard[mapHeight - 1 - i][j].getName().split("-");
							// System.out.println("KLIKNALEM NA: " + nameSplitted[0] + " " + nameSplitted[1]);
							if (nameSplitted[2].contains("Platforms"))
							{
								Array<Vector2> traceToDelete = null;
								for (Array<Vector2> shelfTraces : shelfTrace)
								{
									for (Vector2 shelfTracePoint : shelfTraces)
									{
										if (shelfTracePoint.equals(new Vector2(j, i)))
										{
											traceToDelete = shelfTraces;
										}
									}
								}
								if (traceToDelete != null)
								{
									for (Vector2 toDelete : traceToDelete)
									{
										int yOnMap = mapHeight - 1 - (int) toDelete.y;
										imageCellMainBoard[yOnMap][(int) toDelete.x].setDrawable(new TextureRegionDrawable(textures.get("empty")));
										imageCellMainBoard[yOnMap][(int) toDelete.x].setName((int) toDelete.y + "-" + (int) toDelete.x + "-" + "empty");
										imageCellMainBoard[yOnMap][(int) toDelete.x].additionals.clear();
										imageCellMainBoard[yOnMap][(int) toDelete.x].setPlatformAdditional(false);
									}
									shelfTrace.removeValue(traceToDelete, true);
								}
								else
								// for jumping platforms
								{
									int yOnMap = mapHeight - 1 - i;
									imageCellMainBoard[yOnMap][j].setDrawable(new TextureRegionDrawable(textures.get("empty")));
									imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + "empty");
									imageCellMainBoard[yOnMap][j].additionals.clear();
									imageCellMainBoard[yOnMap][j].setPlatformAdditional(false);
								}
							}
							else
							{
								int yOnMap = mapHeight - 1 - i;
								imageCellMainBoard[yOnMap][j].setDrawable(new TextureRegionDrawable(textures.get("empty")));
								imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + "empty");
								imageCellMainBoard[yOnMap][j].additionals.clear();
								imageCellMainBoard[yOnMap][j].setPlatformAdditional(false);
							}
						}
					}
				});
			}
		}
	}

	private void loadMapFromXMLFile( String xmlName )
	{
		this.clearMap();
		XmlReader reader = new XmlReader();
		try
		{
			FileHandle tempFileHandle = Gdx.files.local("./bin/levels/" + xmlName);
			Element root = reader.parse(tempFileHandle);
			Element tiles = root.getChildByName("tiles");
			Element additionals = root.getChildByName("additionals");
			Element bonuses = root.getChildByName("bonuses");
			Element enemies = root.getChildByName("enemies");
			Element platforms = root.getChildByName("platforms");
			Element jumpPlatforms = root.getChildByName("jumpingPlatforms");
			Element player = root.getChildByName("player");
			Element finalPointEl = root.getChildByName("final");
			this.checkBoxCloudsVisible.setChecked(root.getChildByName("clouds").getBoolean("visible"));
			Array<Element> xmlTile = tiles.getChildrenByName("tile");
			Array<Element> xmlAdditionals = additionals.getChildrenByName("additional");
			Array<Element> xmlBonuses = bonuses.getChildrenByName("bonus");
			Array<Element> xmlEnemies = enemies.getChildrenByName("enemy");
			Array<Element> xmlPlatforms = platforms.getChildrenByName("platform");
			Array<Element> xmlJumpingPlatforms = jumpPlatforms.getChildrenByName("platform");
			// glowne kafelki ktore nie ruszaja sie
			for (Element tile : xmlTile)
			{
				String name = searchFullPathInMap(tile.getAttribute("name"));
				int posX = tile.getInt("posX");
				int posY = tile.getInt("posY");
				int yOnMap = mapHeight - 1 - posY;
				imageCellMainBoard[yOnMap][posX].setDrawable(new TextureRegionDrawable(textures.get(name)));
				imageCellMainBoard[yOnMap][posX].setName(posY + "-" + posX + "-" + name);
			}
			for (Element additional : xmlAdditionals)
			{
				String name = searchFullPathInMap(additional.getAttribute("name"));
				int posX = additional.getInt("posX");
				int posY = additional.getInt("posY");
				int count = 0;
				// check if any additionals is
				for (Element tile : xmlTile)
				{
					if (posX == tile.getInt("posX") && posY == tile.getInt("posY"))
					{
						count++;
						Image additionalTile = new Image(textures.get(name));
						additionalTile.setName(posX + "-" + posY + "-" + name);
						int yOnMap = mapHeight - 1 - posY;
						imageCellMainBoard[yOnMap][posX].additionals.add(additionalTile);
						imageCellMainBoard[yOnMap][posX].setPlatformAdditional(false);
					}
				}
				if (count == 0)
				{
					int yOnMap = mapHeight - 1 - posY;
					imageCellMainBoard[yOnMap][posX].setDrawable(new TextureRegionDrawable(textures.get(name)));
					imageCellMainBoard[yOnMap][posX].setName(posY + "-" + posX + "-" + name);
				}
			}
			for (Element bonus : xmlBonuses)
			{
				String name = searchFullPathInMap(bonus.getAttribute("name"));
				int posX = bonus.getInt("posX");
				int posY = bonus.getInt("posY");
				imageCellMainBoard[mapHeight - 1 - posY][posX].setPlatformAdditional(false);
				int count = 0;
				// check if any additionals is
				for (Element tile : xmlTile)
				{
					if (posX == tile.getInt("posX") && posY == tile.getInt("posY"))
					{
						count++;
						Image additionalTile = new Image(textures.get(name));
						additionalTile.setName(posY + "-" + posX + "-" + name);
						additionalTile.setPosition(posX, posY);
						imageCellMainBoard[mapHeight - 1 - tile.getInt("posX")][tile.getInt("posY")].additionals.add(additionalTile);
					}
				}
				if (count == 0)
				{
					int yOnMap = mapHeight - 1 - posY;
					imageCellMainBoard[yOnMap][posX].setDrawable(new TextureRegionDrawable(textures.get(name)));
					imageCellMainBoard[yOnMap][posX].setName(posY + "-" + posX + "-" + name);
				}
			}
			for (Element enemy : xmlEnemies)
			{
				// BLOCKER, FISH, FLY, SLIME, SNAIL
				String name = searchFullPathInMap(enemy.getAttribute("name"));
				int posX = enemy.getInt("posX");
				int posY = enemy.getInt("posY");
				imageCellMainBoard[mapHeight - 1 - posY][posX].setPlatformAdditional(false);
				int count = 0;
				// check if any additionals is
				for (Element tile : xmlTile)
				{
					if (posX == tile.getInt("posX") && posY == tile.getInt("posY"))
					{
						count++;
						Image additionalTile = new Image(textures.get(name));
						additionalTile.setName(posY + "-" + posX + "-" + name);
						additionalTile.setPosition(posX, posY);
						imageCellMainBoard[mapHeight - 1 - tile.getInt("posX")][this.mapWidth - 1 - tile.getInt("posY")].additionals.add(additionalTile);
					}
				}
				if (count == 0)
				{
					int yOnMap = mapHeight - 1 - posY;
					imageCellMainBoard[yOnMap][posX].setDrawable(new TextureRegionDrawable(textures.get(name)));
					imageCellMainBoard[yOnMap][posX].setName(posY + "-" + posX + "-" + name);
				}
			}
			// moving platforms
			for (Element platform : xmlPlatforms)
			{
				String name = searchFullPathInMap(platform.getAttribute("name"));
				Array<Element> points = platform.getChildrenByName("point");
				shelfTrace.add(new Array<Vector2>());
				for (Element point : points)
				{
					int posX = point.getInt("x");
					int posY = point.getInt("y");
					int yOnMap = mapHeight - 1 - posY;
					imageCellMainBoard[yOnMap][posX].setPlatformAdditional(true);
					EditorTableCell currentCell = imageCellMainBoard[yOnMap][posX];
					this.shelfTrace.get(shelfTrace.size - 1).add(new Vector2(posX, posY));
					currentCell.setDrawable(new TextureRegionDrawable(textures.get(name)));
					currentCell.setName(posY + "-" + posX + "-" + name);
					currentCell.setPlatformAdditional(true);
					// add number as additional
					String number = String.valueOf(this.shelfTrace.get(shelfTrace.size - 1).size / 10);
					Image additional = new Image(textures.get("assets1/Symbols/SYMB" + number));
					additional.setName(imageCellMainBoard[yOnMap][posX].getName());
					additional.setWidth(currentCell.getWidth() / 2);
					additional.setHeight(currentCell.getHeight() / 2);
					additional.setPosition(currentCell.getX(), currentCell.getY());
					imageCellMainBoard[yOnMap][posX].additionals.add(additional);
					number = String.valueOf(this.shelfTrace.get(shelfTrace.size - 1).size % 10);
					additional = new Image(textures.get("assets1/Symbols/SYMB" + number));
					additional.setName(imageCellMainBoard[yOnMap][posX].getName());
					additional.setWidth(currentCell.getWidth() / 2);
					additional.setHeight(currentCell.getHeight() / 2);
					additional.setPosition(currentCell.getX() + (currentCell.getWidth() / 2), currentCell.getY());
					imageCellMainBoard[yOnMap][posX].additionals.add(additional);
				}
			}
			// jumping platforms
			for (Element jumpingPlatforms : xmlJumpingPlatforms)
			{
				String name = searchFullPathInMap(jumpingPlatforms.getAttribute("name"));
				int posX = jumpingPlatforms.getInt("posX");
				int posY = jumpingPlatforms.getInt("posY");
				int yOnMap = mapHeight - 1 - posY;
				imageCellMainBoard[yOnMap][posX].setDrawable(new TextureRegionDrawable(textures.get(name)));
				imageCellMainBoard[yOnMap][posX].setName(posY + "-" + posX + "-" + name);
			}
			// [Y POSITION][X POSITION] !!!!!!!!!!!!!!!
			// init finish point
			int posXFinalPoint = finalPointEl.getInt("posX");
			int posYFinalPoint = finalPointEl.getInt("posY");
			String name = searchFullPathInMap("signExit");
			int yOnMap = mapHeight - 1 - posYFinalPoint;
			imageCellMainBoard[yOnMap][posXFinalPoint].setDrawable(new TextureRegionDrawable(textures.get(name)));
			imageCellMainBoard[yOnMap][posXFinalPoint].setName(posYFinalPoint + "-" + posXFinalPoint + "-" + name);
			imageCellMainBoard[yOnMap][posXFinalPoint].setPlatformAdditional(false);
			// initialize player
			int playerPosX = player.getInt("posX");
			int playerPosY = player.getInt("posY");
			initialPosition.set(playerPosX,playerPosY);
			name = searchFullPathInMap("signStart");
			yOnMap = mapHeight - 1 - playerPosY;
			imageCellMainBoard[yOnMap][playerPosX].setDrawable(new TextureRegionDrawable(textures.get(name)));
			imageCellMainBoard[yOnMap][playerPosX].setName(playerPosY + "-" + playerPosX + "-" + name);
			imageCellMainBoard[yOnMap][playerPosX].setPlatformAdditional(false);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void dispose()
	{
		pixMapBlue.dispose();
		sb.dispose();
		stage.dispose();
	}

	private void clearMap()
	{
		for (int i = 0; i < this.mapHeight; i++)
		{
			for (int j = 0; j < this.mapWidth; j++)
			{
				int yOnMap = mapHeight - 1 - i;
				if (imageCellMainBoard[yOnMap][j].additionals.size != 0)
				{
					imageCellMainBoard[yOnMap][j].additionals.clear();
				}
				imageCellMainBoard[yOnMap][j].setDrawable(new TextureRegionDrawable(textures.get("empty")));
				imageCellMainBoard[yOnMap][j].setName(i + "-" + j + "-" + "empty");
				imageCellMainBoard[yOnMap][j].setPlatformAdditional(false);
			}
		}
		this.shelfTrace.clear();
		this.tempTrace.clear();
	}

	private boolean isBeginPointSet()
	{
		for (int i = 0; i < this.mapHeight; i++)
		{
			for (int j = 0; j < this.mapWidth; j++)
			{
				if (imageCellMainBoard[i][j].getName().toUpperCase().contains("SIGNSTART"))
				{
					return true;
				}
			}
		}
		return false;
	}

	private boolean isEndPointSet()
	{
		for (int i = 0; i < this.mapHeight; i++)
		{
			for (int j = 0; j < this.mapWidth; j++)
			{
				if (imageCellMainBoard[i][j].getName().toUpperCase().contains("SIGNEXIT"))
				{
					return true;
				}
			}
		}
		return false;
	}

	private String searchFullPathInMap( String inputString )
	{
		String minString = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"; // simple
																					// example
																					// :)
		for (Entry<String, AtlasRegion> entry : MyAssetsManager.instance.textures.entrySet())
		{
			if (entry.getKey().toUpperCase().contains(inputString.toUpperCase()))
			{
				if (entry.getKey().length() < minString.length())
				{
					minString = entry.getKey();
				}
			}
		}
		if (inputString.compareTo(minString) != 0)
			return minString;
		else
			return inputString;
	}
}
