
package pl.majority.game.screens;

import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

public class LevelSelectScreen extends AbstractScreen
{
	private TextureRegionDrawable buttonBackground;
	private Stage stage;
	private TextureRegion backgroundImage;
	private TextButton buttonGoBack;
	private Table rootTable, horizontalGroupLevelsTable;
	public Skin skin;
	private Label labelMain;
	private SpriteBatch sb;
	// private HorizontalGroup horizontalGroupLevelsTable;
	private ScrollPane scrollPaneLevels;

	public LevelSelectScreen( Game game )
	{
		super(game);
		skin = MyAssetsManager.instance.skin;
		buttonGoBack = new TextButton("Go back", skin, "mainSetting");
		horizontalGroupLevelsTable = new Table();
		scrollPaneLevels = new ScrollPane(horizontalGroupLevelsTable, skin, "scrollPaneAssets");
		scrollPaneLevels.setScrollingDisabled(false, true);
		backgroundImage = MyAssetsManager.instance.textures.get("backgroundImage");
		stage = new Stage(new FillViewport (Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		sb = new SpriteBatch();
		rootTable = new Table();
		labelMain = new Label("Select level", skin, "labelGameTitle");
		buttonBackground = new TextureRegionDrawable(MyAssetsManager.instance.textures.get("mainButton"));
	}

	@Override
	public void render( float deltaTime )
	{
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		sb.begin();
		sb.draw(backgroundImage, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		sb.end();
		stage.act(deltaTime);
		stage.draw();
	}

	@Override
	public void resize( int width, int height )
	{
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show()
	{
		buttonGoBack.addListener(new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				if (GamePreferences.getInstance().sound)
				{
					MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
				}
				game.setScreen(new MenuScreen(game));
			}
		});
		rootTable.add(labelMain).center().padTop(20);
		rootTable.row();
		rootTable.add(scrollPaneLevels).expand().center();
		rootTable.row();
		// init internal levels list
		FileHandle dirHandle = Gdx.files.local("./bin/levels/");
		for (FileHandle entry : dirHandle.list())
		{
			if (entry.file().getName().toUpperCase().contains("XML"))
			{
				String filename = entry.file().getName();
				final TextButton tb = new TextButton(filename.substring(0, filename.length() - 4), skin, "mainSetting");
				tb.setBackground(buttonBackground);
				tb.setName(filename.substring(0, filename.length() - 4));
				// tb.padRight(50);
				tb.addListener(new ClickListener()
				{
					@Override
					public void clicked( InputEvent event, float x, float y )
					{
						if (GamePreferences.getInstance().sound)
						{
							MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
						}
						game.setScreen(new GameScreen(game, tb.getText() + ".xml"));
					}
				});
				horizontalGroupLevelsTable.add(tb).width((tb.getName().length() * 20) + 20).height(125).pad(5);
			}
		}
		if (dirHandle.list().length == 0)
		{
			horizontalGroupLevelsTable.add(new Label("NO MAPS..", skin, "labelGameTitle"));
		}
		rootTable.row();
		rootTable.add(buttonGoBack).size(100, 60).bottom().center();
		rootTable.setFillParent(true);
		stage.addActor(rootTable);
		Gdx.input.setInputProcessor(stage);
		if (GamePreferences.getInstance().drawDebug)
		{
			for (Actor actorInStage : stage.getActors())
			{
				actorInStage.setDebug(true);
			}
		}
	}

	@Override
	public void hide()
	{
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void dispose()
	{
		stage.dispose();
	}
}
