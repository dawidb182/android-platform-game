
package pl.majority.game.screens;

import pl.majority.game.GameLogic;
import pl.majority.game.GameRenderer;
import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen extends AbstractScreen
{
	private GameLogic gameLogic;
	private GameRenderer gameRenderer;
	private static boolean paused = false;
	private String mapPath;

	public GameScreen( Game game, CharSequence charSequence )
	{
		super(game);
		mapPath = charSequence.toString();
	}

	@Override
	public void render( float deltaTime )
	{
		if (!isPaused())
		{
			gameLogic.update(deltaTime);
		}
		Gdx.gl.glClearColor(0f, 152 / 255f, 1f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gameRenderer.render();
	}

	@Override
	public void resize( int width, int height )
	{
		gameRenderer.resize(width, height);
	}

	@Override
	public void show()
	{
		GamePreferences.getInstance().load();
		gameLogic = new GameLogic(game, mapPath);
		gameRenderer = new GameRenderer(gameLogic);
		if (MyAssetsManager.instance.mainScreenMusic.isPlaying())
		{
			MyAssetsManager.instance.mainScreenMusic.stop();
		}
		if (GamePreferences.getInstance().music)
		{
			MyAssetsManager.instance.gameMusicScreen.setVolume(GamePreferences.getInstance().volMusic);
			MyAssetsManager.instance.gameMusicScreen.setLooping(true);
			MyAssetsManager.instance.gameMusicScreen.play();
		}
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void hide()
	{
		Gdx.input.setCatchBackKey(false);
		//setPaused(true);
	}

	@Override
	public void pause()
	{
		setPaused(true);
	}

	@Override
	public void resume()
	{
		super.resume();
		setPaused(false);
	}

	@Override
	public void dispose()
	{
		gameLogic.dispose();
		gameRenderer.dispose();
	}

	public static boolean isPaused()
	{
		return paused;
	}

	public static void setPaused( boolean paused )
	{
		GameScreen.paused = paused;
	}
}
