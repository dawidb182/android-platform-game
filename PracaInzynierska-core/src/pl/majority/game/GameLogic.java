
package pl.majority.game;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import pl.majority.game.objects.Enemy.ENEMY_TYPE;
import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.objects.Enemy;
import pl.majority.game.objects.FireBall;
import pl.majority.game.objects.GoldCoin;
import pl.majority.game.objects.JumpingTrampoline;
import pl.majority.game.objects.Level;
import pl.majority.game.objects.MovingPlatform;
import pl.majority.game.objects.Player.PLAYER_STATE;
import pl.majority.game.objects.Tile;
import pl.majority.game.screens.LevelSelectScreen;
import pl.majority.game.util.CameraHelper;
import pl.majority.game.util.GamePreferences;
import pl.majority.game.util.MyContactListener;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.AudioRecorder;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.musicg.api.WhistleApi;
import com.musicg.wave.WaveHeader;

public class GameLogic extends GestureDetector implements Disposable
{
	private Game game;
	public Level level;
	public float scoreVisual;
	public float livesVisual;
	public float TIME_DELAY_GAME_OVER = 3;
	public World box2dWorld;
	public CameraHelper cameraHelper;
	private boolean isGameOverSoundPlayed;
	// for sound analyzer
	private static final int samples = 2048;
	public Thread thread;
	private WaveHeader waveHeader;
	private WhistleApi whistleApi;
	public static boolean whistleIsDetect = false;
	private int whistleCounter;
	private AudioRecorder audio;
	private static int currentLookingMonster = -1;

	public GameLogic( Game game, String levelPath )
	{
		super(new GestureAdapter()
		{
			@Override
			public boolean fling( float velocityX, float velocityY, int button )
			{
				if (Gdx.input.getY() > 150 && Gdx.input.getY() < Gdx.graphics.getHeight() - 150)
				{
					if (Math.abs(velocityX) > Math.abs(velocityY))
					{
						if (velocityX > 0)
						{
							// RIGHT
							currentLookingMonster++;
						}
						else
						{
							// LEFT
							currentLookingMonster--;
						}
					}
					else
					{
						if (velocityY > 0)
						{
							// DOWN
							currentLookingMonster = -1;
						}
						else
						{
							// UP
							currentLookingMonster = -1;
						}
					}
				}
				return super.fling(velocityX, velocityY, button);
			}
		});
		whistleCounter = 0;
		this.game = game;
		scoreVisual = 0;
		isGameOverSoundPlayed = false;
		Gdx.input.setInputProcessor(this);
		cameraHelper = new CameraHelper();
		level = new Level(levelPath);
		livesVisual = level.player.lives;
		cameraHelper.setTarget(level.player);
		// //////////////// init BOX2D engine///////////////////////
		if (box2dWorld != null)
		{
			box2dWorld.dispose();
		}
		// create b2d world
		box2dWorld = new World(new Vector2(0, -9.81f), true);
		// set my own contact listener :)
		box2dWorld.setContactListener(new MyContactListener());
		// box2d collision filtering categories and masks
		final short CATEGORY_GROUND = 0x0001;
		final short CATEGORY_PLATFORM = 0x0002;
		final short CATEGORY_TRAMPOLINE = 0x0004;
		final short CATEGORY_PLAYER_RECT = 0x0008;
		final short CATEGORY_PLAYER_LINE = 0x0016;
		final short CATEGORY_ENEMY = 0x0032;
		final short CATEGORY_ADDITIONAL = 0x0064;
		final short CATEGORY_FIREBALL = 0x0128;
		final short CATEGORY_FINAL_POINT = 0x0256;
		final short MASK_GROUND = CATEGORY_ENEMY | CATEGORY_PLAYER_LINE | CATEGORY_PLAYER_RECT;
		final short MASK_PLATFORM = CATEGORY_PLAYER_LINE;
		final short MASK_TRAMPOLINE = CATEGORY_PLAYER_RECT | CATEGORY_PLAYER_LINE;
		final short MASK_PLAYER_RECT = CATEGORY_GROUND | CATEGORY_ENEMY | CATEGORY_ADDITIONAL | CATEGORY_FINAL_POINT;
		final short MASK_PLAYER_LINE = CATEGORY_GROUND | CATEGORY_ENEMY | CATEGORY_ADDITIONAL | CATEGORY_FINAL_POINT | CATEGORY_PLATFORM;
		final short MASK_ENEMY = CATEGORY_GROUND | CATEGORY_PLAYER_RECT | CATEGORY_PLAYER_LINE | CATEGORY_FIREBALL | CATEGORY_ENEMY; // enemies can't collide //
																																		// with moving platforms
		final short MASK_ADDITIONAL = CATEGORY_PLAYER_RECT | CATEGORY_PLAYER_LINE;
		final short MASK_FIREBALL = CATEGORY_ENEMY | CATEGORY_GROUND;
		final short MASK_FINAL_POINT = CATEGORY_PLAYER_RECT | CATEGORY_PLAYER_LINE;
		// Rocks box2d
		Vector2 origin = new Vector2();
		for (Tile tile : level.tilesBoard)
		{
			BodyDef bodyDefTile = new BodyDef();
			bodyDefTile.type = BodyType.StaticBody;
			bodyDefTile.position.set(tile.position);
			Body body = box2dWorld.createBody(bodyDefTile);
			tile.body = body;
			PolygonShape polygonShape = new PolygonShape();
			// from left to right triangle
			if (tile.texPath.toUpperCase().contains("LEFTED"))
			{
				Vector2[] vertices = new Vector2[3];
				vertices[0] = new Vector2(0.0f, 0.0f);
				vertices[1] = new Vector2(1.0f, 0.0f);
				vertices[2] = new Vector2(1.0f, 1.0f);
				polygonShape.set(vertices);
				FixtureDef fixtureDefRock = new FixtureDef();
				fixtureDefRock.filter.categoryBits = CATEGORY_GROUND;
				fixtureDefRock.filter.maskBits = MASK_GROUND;
				fixtureDefRock.friction = 0.5f;
				fixtureDefRock.shape = polygonShape;
				// fixtureDefRock.density = 0.5f;
				body.createFixture(fixtureDefRock).setUserData("TILELEFT");
			}
			else
				// from right to left triangle
				if (tile.texPath.toUpperCase().contains("RIGHTED"))
				{
					Vector2[] vertices = new Vector2[3];
					vertices[0] = new Vector2(0.0f, 0.0f);
					vertices[1] = new Vector2(1.0f, 0.0f);
					vertices[2] = new Vector2(0.0f, 1.0f);
					polygonShape.set(vertices);
					FixtureDef fixtureDefRock = new FixtureDef();
					fixtureDefRock.filter.categoryBits = CATEGORY_GROUND;
					fixtureDefRock.filter.maskBits = MASK_GROUND;
					fixtureDefRock.friction = 0.5f;
					fixtureDefRock.shape = polygonShape;
					// fixtureDefRock.density = 0.5f;
					body.createFixture(fixtureDefRock).setUserData("TILERIGHT");
				}
				else
				{
					origin.x = tile.bounds.width / 2f;
					origin.y = tile.bounds.height / 2f;
					polygonShape.setAsBox(origin.x, origin.y, origin, 0);
					FixtureDef fixtureDefRock = new FixtureDef();
					fixtureDefRock.filter.categoryBits = CATEGORY_GROUND;
					fixtureDefRock.filter.maskBits = MASK_GROUND;
					fixtureDefRock.friction = 0.5f;
					fixtureDefRock.shape = polygonShape;
					// fixtureDefRock.density = 0.5f;
					body.createFixture(fixtureDefRock).setUserData("TILE");
				}
			polygonShape.dispose();
		}
		// gold coins box2D
		for (GoldCoin goldCoin : level.goldCoins)
		{
			BodyDef bodyDefGoldCoin = new BodyDef();
			bodyDefGoldCoin.type = BodyType.StaticBody;
			bodyDefGoldCoin.position.set(goldCoin.position);
			Body goldCoinBody = box2dWorld.createBody(bodyDefGoldCoin);
			goldCoin.body = goldCoinBody;
			PolygonShape polygonShapeGoldCoin = new PolygonShape();
			origin.x = goldCoin.bounds.width * 0.5f;
			origin.y = goldCoin.bounds.height * 0.5f;
			polygonShapeGoldCoin.setAsBox(origin.x / 2, origin.y / 2, origin, 0);
			FixtureDef fixtureDefGoldCoin = new FixtureDef();
			fixtureDefGoldCoin.filter.categoryBits = CATEGORY_ADDITIONAL;
			fixtureDefGoldCoin.filter.maskBits = MASK_ADDITIONAL;
			fixtureDefGoldCoin.shape = polygonShapeGoldCoin;
			fixtureDefGoldCoin.isSensor = true;
			goldCoinBody.createFixture(fixtureDefGoldCoin).setUserData(goldCoin);
			polygonShapeGoldCoin.dispose();
		}
		// jumping platforms
		for (JumpingTrampoline jt : level.jumpingTrampolines)
		{
			BodyDef bodyDefJumpingTrampoline = new BodyDef();
			bodyDefJumpingTrampoline.type = BodyType.StaticBody;
			bodyDefJumpingTrampoline.position.set(jt.position);
			Body goldCoinBody = box2dWorld.createBody(bodyDefJumpingTrampoline);
			jt.body = goldCoinBody;
			PolygonShape polygonShapeJumpingTrampoline = new PolygonShape();
			origin.x = jt.bounds.width * 0.5f;
			origin.y = jt.bounds.height * 0.5f;
			polygonShapeJumpingTrampoline.setAsBox(origin.x, origin.y, origin, 0);
			FixtureDef fixtureDefJumpingTrampoline = new FixtureDef();
			fixtureDefJumpingTrampoline.filter.categoryBits = CATEGORY_TRAMPOLINE;
			fixtureDefJumpingTrampoline.filter.maskBits = MASK_TRAMPOLINE;
			fixtureDefJumpingTrampoline.shape = polygonShapeJumpingTrampoline;
			fixtureDefJumpingTrampoline.isSensor = true;
			goldCoinBody.createFixture(fixtureDefJumpingTrampoline).setUserData(jt);
			polygonShapeJumpingTrampoline.dispose();
		}
		// fireballs box2d
		for (FireBall fireBall : level.player.fireBalls)
		{
			fireBall.canThrow = true;
			BodyDef bodyDefFireBall = new BodyDef();
			bodyDefFireBall.type = BodyType.DynamicBody;
			bodyDefFireBall.position.set(fireBall.position);
			Body fireballBody = box2dWorld.createBody(bodyDefFireBall);
			fireballBody.setActive(false);
			fireBall.body = fireballBody;
			CircleShape circleShapeFireBall = new CircleShape();
			circleShapeFireBall.setPosition(new Vector2(0.3f, 0.3f));
			circleShapeFireBall.setRadius(0.1f);
			FixtureDef fixtureDefFireBall = new FixtureDef();
			fixtureDefFireBall.filter.categoryBits = CATEGORY_FIREBALL;
			fixtureDefFireBall.filter.maskBits = MASK_FIREBALL;
			fixtureDefFireBall.shape = circleShapeFireBall;
			fireballBody.createFixture(fixtureDefFireBall).setUserData(fireBall);
			circleShapeFireBall.dispose();
		}
		// enemies box2d
		for (Enemy enemy : level.enemies)
		{
			BodyDef bodyDefEnemy = new BodyDef();
			if(enemy.enemyType.equals(ENEMY_TYPE.BLOCKER))
			{
				bodyDefEnemy.type = BodyType.StaticBody;
			}
			else
			{
				bodyDefEnemy.type = BodyType.DynamicBody;
			}
			bodyDefEnemy.position.set(enemy.position);
			Body enemyBody = box2dWorld.createBody(bodyDefEnemy);
			enemy.body = enemyBody;
			enemyBody.setActive(true);
			CircleShape circleShapeEnemy = new CircleShape();
			circleShapeEnemy.setPosition(new Vector2(enemy.origin.x, enemy.origin.y));
			circleShapeEnemy.setRadius(0.40f);
			FixtureDef fixtureDefEnemy = new FixtureDef();
			fixtureDefEnemy.filter.categoryBits = CATEGORY_ENEMY;
			fixtureDefEnemy.filter.maskBits = MASK_ENEMY;
			fixtureDefEnemy.isSensor = false;// fixtureDefEnemy.isSensor = true;
			fixtureDefEnemy.shape = circleShapeEnemy;
			enemyBody.createFixture(fixtureDefEnemy).setUserData(enemy);
			circleShapeEnemy.dispose();
		}
		// player box2d
		BodyDef bodyDefPlayer = new BodyDef();
		bodyDefPlayer.type = BodyType.DynamicBody;
		bodyDefPlayer.position.set(level.player.position);
		// bodyDefPlayer.bullet = true;
		Body bodyPlayer = box2dWorld.createBody(bodyDefPlayer);
		level.player.body = bodyPlayer;
		EdgeShape edgeShape = new EdgeShape();
		// b2Vec2 v0(1.7f, 0.0f);
		// b2Vec2 v1(1.0f, 0.25f);
		// b2Vec2 v2(0.0f, 0.0f);
		// b2Vec2 v3(-1.7f, 0.4f);
		Vector2 vect1edgeShape = new Vector2(0.555f, 0.0f);
		Vector2 vect2edgeShape = new Vector2(0.445f, 0.0f);
		Vector2 vect0edgeShape = new Vector2(0.4f, 0.4f);
		Vector2 vect3edgeShape = new Vector2(-0.4f, -0.4f);
		edgeShape.set(vect1edgeShape, vect2edgeShape);
		edgeShape.setVertex0(vect0edgeShape);
		edgeShape.setVertex3(vect3edgeShape);
		FixtureDef fixtureDefPlayer = new FixtureDef();
		fixtureDefPlayer.isSensor = false;
		fixtureDefPlayer.shape = edgeShape;
		fixtureDefPlayer.filter.categoryBits = CATEGORY_PLAYER_LINE;
		fixtureDefPlayer.filter.maskBits = MASK_PLAYER_LINE;
		bodyPlayer.createFixture(fixtureDefPlayer).setUserData(level.player);
		bodyPlayer.setBullet(true);
		PolygonShape psPlayerRect = new PolygonShape();
		origin.x = level.player.bounds.width / 2;
		origin.y = level.player.bounds.height / 2 - 0.1f;
		psPlayerRect.setAsBox(origin.x / 8, origin.y * 0.9f, origin, 0);
		FixtureDef fixtureDefPlayerRect = new FixtureDef();
		fixtureDefPlayerRect.filter.categoryBits = CATEGORY_PLAYER_RECT;
		fixtureDefPlayerRect.filter.maskBits = MASK_PLAYER_RECT;
		fixtureDefPlayerRect.isSensor = false;
		fixtureDefPlayerRect.shape = psPlayerRect;
		bodyPlayer.createFixture(fixtureDefPlayerRect).setUserData("PLAYER_RECT");
		psPlayerRect.dispose();
		// finalPoint box2d
		BodyDef bodyDefFinalPoint = new BodyDef();
		bodyDefFinalPoint.type = BodyType.StaticBody;
		bodyDefFinalPoint.position.set(level.finalPoint.position);
		Body bodyFinalPoint = box2dWorld.createBody(bodyDefFinalPoint);
		level.finalPoint.body = bodyFinalPoint;
		PolygonShape psFinalPoint = new PolygonShape();
		origin.x = level.finalPoint.bounds.width / 2;
		origin.y = level.finalPoint.bounds.height / 2;
		psFinalPoint.setAsBox(origin.x, origin.y, origin, 0);
		FixtureDef fixtureDefFinalPoint = new FixtureDef();
		fixtureDefFinalPoint.shape = psFinalPoint;
		fixtureDefFinalPoint.isSensor = true;
		fixtureDefPlayerRect.filter.categoryBits = CATEGORY_FINAL_POINT;
		fixtureDefPlayerRect.filter.maskBits = MASK_FINAL_POINT;
		bodyFinalPoint.createFixture(fixtureDefFinalPoint).setUserData(level);
		psFinalPoint.dispose();
		edgeShape.dispose();
		// moving platforms
		for (MovingPlatform movingPlatform : level.movingPlatformsArray)
		{
			BodyDef bodyDefMovingPlatform = new BodyDef();
			bodyDefMovingPlatform.type = BodyType.KinematicBody;
			bodyDefMovingPlatform.position.set(movingPlatform.position);
			Body movingPlatformBody = box2dWorld.createBody(bodyDefMovingPlatform);
			movingPlatform.body = movingPlatformBody;
			PolygonShape polygonShapeMovingPlatform = new PolygonShape();
			Vector2 leftBottom = new Vector2(0, 0.5f);
			Vector2 leftTop = new Vector2(0, 1);
			Vector2 rightTop = new Vector2(1, 1);
			Vector2 rightBottom = new Vector2(1, 0.5f);
			Vector2[] array =
			{leftBottom, leftTop, rightTop, rightBottom};
			polygonShapeMovingPlatform.set(array);
			// EdgeShape polygonShapeMovingPlatform = new EdgeShape();
			// polygonShapeMovingPlatform.set(new Vector2(0,1), new Vector2(1,1));
			FixtureDef fixtureDefMovingPlatform = new FixtureDef();
			fixtureDefMovingPlatform.shape = polygonShapeMovingPlatform;
			fixtureDefMovingPlatform.isSensor = false;
			fixtureDefMovingPlatform.friction = 0.2f;
			fixtureDefMovingPlatform.filter.categoryBits = CATEGORY_PLATFORM;
			fixtureDefMovingPlatform.filter.maskBits = MASK_PLATFORM;
			movingPlatformBody.setBullet(true);
			movingPlatformBody.createFixture(fixtureDefMovingPlatform).setUserData(movingPlatform);
			polygonShapeMovingPlatform.dispose();
		}
		// //initialize sound analyzer
		audio = MyAssetsManager.instance.audio;
		waveHeader = new WaveHeader();
		waveHeader.setChannels(1);
		waveHeader.setBitsPerSample(16);
		waveHeader.setSampleRate(44100);
		whistleApi = new WhistleApi(waveHeader);
		final Runnable runnableSendResults = new Runnable()
		{
			@Override
			public void run()
			{
				if (whistleCounter > 3)
				{
					whistleIsDetect = true;
				}
				else
				{
					whistleIsDetect = false;
				}

			}
		};
		thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				while (thread != null && !thread.isInterrupted())
				{
					// read audio buffer
					// gets the sound level out of the sample
					try
					{
						if (audio != null)
						{
							short[] buffer = new short[samples];
							audio.read(buffer, 0, samples);
							byte[] bytesBuffer = new byte[buffer.length * 2];
							ByteBuffer.wrap(bytesBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(buffer);
							
							if (whistleApi.isWhistle(bytesBuffer))
							{
								whistleCounter++;
							}
							else
							{
								if (whistleCounter > 0)
								{
									whistleCounter--;
								}
							}
						}
						// To pass data to the rendering thread from another thread we recommend using Application.postRunnable().
						Gdx.app.postRunnable(runnableSendResults);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		});
		thread.start();
	}

	@Override
	public void dispose()
	{
		if (box2dWorld != null)
			box2dWorld.dispose();
	}

	public void update( float deltaTime )
	{
		if (currentLookingMonster != -1)
		{
			if (currentLookingMonster < level.enemies.size && currentLookingMonster > 0)
			{
				cameraHelper.setTarget(level.enemies.get(currentLookingMonster));
			}
		}
		else
		{
			cameraHelper.setTarget(level.player);
		}
		// in case of players running after
		if (level.isComplete() && !isPlayerUnderGround())
		{
			disableAllBodies();
			if (MyAssetsManager.instance.gameMusicScreen.isPlaying())
			{
				MyAssetsManager.instance.gameMusicScreen.stop();
			}
			if (GamePreferences.getInstance().sound && !isGameOverSoundPlayed)
			{
				MyAssetsManager.instance.applause.play(GamePreferences.getInstance().volSound);
				isGameOverSoundPlayed = true;
			}
			if (Gdx.input.justTouched())
			{
				this.backToMenu();
			}
			return;
		}
		if (!isGameOver())
		{
			handleGameInput(deltaTime);
		}
		else
		{
			this.level.player.playerState = PLAYER_STATE.STANDING;
			disableAllBodies();
			if (GamePreferences.getInstance().sound && !this.isGameOverSoundPlayed)
			{
				if (MyAssetsManager.instance.gameMusicScreen.isPlaying())
				{
					MyAssetsManager.instance.gameMusicScreen.stop();
				}
				MyAssetsManager.instance.dead.play(GamePreferences.getInstance().volSound);
				isGameOverSoundPlayed = true;
			}
			// go back to main menu after game over
			if (Gdx.input.justTouched())
			{
				this.backToMenu();
			}
			return;
		}
		// BOX2D TEST
		box2dWorld.step(deltaTime, 8, 3);
		level.update(deltaTime);
		cameraHelper.update(deltaTime);
		if (!isGameOver() && isPlayerUnderGround())
		{
			level.player.lives--;
			// transform player to initial position after moving under ground
			level.player.body.setTransform(level.player.initialPosition.x, level.player.initialPosition.y, 0);
		}
		if (livesVisual > level.player.lives)
		{
			livesVisual = Math.max(level.player.lives, livesVisual - 1 * deltaTime);
		}
		if (scoreVisual < level.player.score)
		{
			scoreVisual = Math.min(level.player.score, scoreVisual + 250 * deltaTime);
		}
	}

	private void disableAllBodies()
	{
		// disable all bodies in world
		Array<Body> bodies = new Array<Body>();
		box2dWorld.getBodies(bodies);
		for (Body b : bodies)
		{
			b.setActive(false);
		}
	}

	private void handleGameInput( float deltaTime )
	{
		Vector2 playerPosition = level.player.body.getPosition();
		Vector2 playerVelocity = level.player.body.getLinearVelocity();
		float maxVelocityX = level.player.maxVelocityWalking.x;
		if (!level.isComplete())
		{
			if (level.player.playerState != PLAYER_STATE.THROWING)
			{
				if (Gdx.input.isKeyPressed(Keys.LEFT) || GameRenderer.leftButtonOnScreenIsPressed)
				{
					if (playerVelocity.x > -maxVelocityX && (level.player.playerState != PLAYER_STATE.THROWING))
					{
						if (level.player.getPlatformReference() != null)
						{
							level.player.body.applyLinearImpulse(-2f, 0, playerPosition.x, playerPosition.y, true);
						}
						else
							if (level.player.body.getLinearVelocity().y == 0.0f)
							{
								level.player.body.applyLinearImpulse(-0.15f, 0f, playerPosition.x, playerPosition.y, true);
							}
							else
							{
								level.player.body.applyLinearImpulse(-0.3f, 0f, playerPosition.x, playerPosition.y, true);
							}
					}
				}
				if (Gdx.input.isKeyPressed(Keys.RIGHT) || GameRenderer.rightButtonOnScreenIsPressed)
				{
					if (playerVelocity.x < maxVelocityX && (level.player.playerState != PLAYER_STATE.THROWING))
					{
						if (level.player.getPlatformReference() != null)
						{
							level.player.body.applyLinearImpulse(2f, 0, playerPosition.x, playerPosition.y, true);
						}
						else
							if (level.player.body.getLinearVelocity().y == 0.0f)
							{
								level.player.body.applyLinearImpulse(0.15f, 0, playerPosition.x, playerPosition.y, true);
							}
							else
							{
								level.player.body.applyLinearImpulse(0.3f, 0, playerPosition.x, playerPosition.y, true);
							}
					}
				}
				// Player Jump
				if (Gdx.input.isKeyPressed(Keys.SPACE))
				{
					level.player.setJumping();
				}
			}
		}
	}

	public void killAllMonstersFromScreen()
	{
		int camX = (int) cameraHelper.getPosition().x;
		int camY = (int) cameraHelper.getPosition().y;
		
		for (int i = camX - 5; i <= camX + 5; i++)
		{
			for (int j = camY - 3; j <= camY + 3; j++)
			{
				for (Enemy e : level.enemies)
				{
					int enemyX = (int) e.body.getPosition().x;
					int enemyY = (int) e.body.getPosition().y;
					if (enemyX == i && enemyY == j)
					{
						e.setEnemyAsDead();
					}
				}
			}
		}
	}

	private void backToMenu()
	{
		game.setScreen(new LevelSelectScreen(game));
		if (!MyAssetsManager.instance.mainScreenMusic.isPlaying() && GamePreferences.getInstance().music)
		{
			MyAssetsManager.instance.mainScreenMusic.play();
		}
	}

	public boolean isGameOver()
	{
		return level.player.lives < 0;
	}

	private boolean isPlayerUnderGround()
	{
		return level.player.position.y < 0;
	}
}
