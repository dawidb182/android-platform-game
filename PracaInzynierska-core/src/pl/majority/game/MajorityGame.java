package pl.majority.game;

import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.screens.MenuScreen;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;

public class MajorityGame extends Game
{

	@Override
	public void create()
	{
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		Gdx.input.setCatchBackKey(true); // DO NOT CLOSE
		MyAssetsManager.instance.init(new AssetManager()); // initialize created assets
		GamePreferences.getInstance().load(); // initialize settings
		setScreen(new MenuScreen(this));
	}
	
	@Override
	public void dispose()
	{
		super.dispose();
		MyAssetsManager.instance.dispose();
		Gdx.app.exit();
	}

}
