
package pl.majority.game;

import pl.majority.game.objects.MyAssetsManager;
import pl.majority.game.objects.Enemy;
import pl.majority.game.objects.GoldCoin;
import pl.majority.game.objects.Player;
import pl.majority.game.screens.GameScreen;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.Disposable;

public class GameRenderer implements Disposable
{
	private OrthographicCamera camera;
	private OrthographicCamera cameraGUI;
	private TextureRegion btnLeft, btnRight, btnJump, btnThrow, btnPause;
	private Rectangle btnLeftRect, btnRightRect, btnJumpRect, btnThrowRect, btnPauseRect;
	private FPSLogger fpslog;
	private SpriteBatch batch;
	private GameLogic gameLogic;
	// Visible game world is 10 meters wide
	public static final float VIEWPORT_WIDTH = 10.0f;
	// Visible game world is 6 meters tall
	public static final float VIEWPORT_HEIGHT = 6.0f;
	public static final float VIEWPORT_GUI_WIDTH = 800f;
	public static final float VIEWPORT_GUI_HEIGHT = 480f;
	public static boolean leftButtonOnScreenIsPressed, rightButtonOnScreenIsPressed;
	private ParticleEffect effectShoutExplosion;
	private boolean canPlayBlahBlahBlah;
	// BOX2D renderer
	private Box2DDebugRenderer b2debugRenderer;
	private float timeForExplosion;
	private boolean canDrawExplosion;
	static float whistleTimerInSeconds;
	public static boolean wereFlipped = false;

	public GameRenderer( GameLogic worldController )
	{
		canDrawExplosion = false;
		timeForExplosion = 0.0f;
		canPlayBlahBlahBlah = false;
		this.gameLogic = worldController;
		fpslog = new FPSLogger();
		leftButtonOnScreenIsPressed = false;
		rightButtonOnScreenIsPressed = false;
		batch = new SpriteBatch();
		camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		camera.position.set(0, 0, 0);
		camera.update();
		cameraGUI = new OrthographicCamera(VIEWPORT_GUI_WIDTH, VIEWPORT_GUI_HEIGHT);
		cameraGUI.position.set(0, 0, 0);
		cameraGUI.setToOrtho(true);
		cameraGUI.update();
		b2debugRenderer = new Box2DDebugRenderer();
		// init onscreen buttons
		btnLeft = MyAssetsManager.instance.textures.get("assets1/shadedDark05");
		btnRight = MyAssetsManager.instance.textures.get("assets1/shadedDark06");
		btnThrow = MyAssetsManager.instance.textures.get("assets1/shadedDark36");
		btnJump = MyAssetsManager.instance.textures.get("assets1/shadedDark37");
		btnPause = MyAssetsManager.instance.textures.get("assets1/shadedDark44");
		if (!wereFlipped)
		{
			wereFlipped = true;
			btnLeft.flip(false, true);
			btnRight.flip(false, true);
			btnThrow.flip(false, true);
			btnJump.flip(false, true);
			btnPause.flip(false, true);
		}
		btnLeftRect = new Rectangle(15, cameraGUI.viewportHeight - 15 - btnLeft.getRegionHeight(), btnLeft.getRegionWidth(), btnLeft.getRegionHeight());
		btnRightRect = new Rectangle(15 + btnLeft.getRegionWidth() + 3, cameraGUI.viewportHeight - 15 - btnRight.getRegionHeight(), btnRight.getRegionWidth(),
				btnRight.getRegionHeight());
		btnJumpRect = new Rectangle(cameraGUI.viewportWidth - 15 - btnJump.getRegionWidth(), cameraGUI.viewportHeight - 15 - btnJump.getRegionHeight(),
				btnJump.getRegionWidth(), btnJump.getRegionHeight());
		btnThrowRect = new Rectangle(cameraGUI.viewportWidth - 15 - btnJump.getRegionWidth() - btnThrow.getRegionWidth() - 3, cameraGUI.viewportHeight - 15
				- btnThrow.getRegionHeight(), btnThrow.getRegionWidth(), btnThrow.getRegionHeight());
		btnPauseRect = new Rectangle(cameraGUI.viewportWidth - 15 - btnPause.getRegionWidth(), 15 + btnPause.getRegionHeight(), btnPause.getRegionWidth(),
				btnPause.getRegionHeight());
		effectShoutExplosion = new ParticleEffect();
		effectShoutExplosion.load(Gdx.files.internal("effects/shoutExplosion.p"), Gdx.files.internal(""));
		effectShoutExplosion.setPosition(worldController.cameraHelper.getPosition().x, worldController.cameraHelper.getPosition().y);
		effectShoutExplosion.start();
		whistleTimerInSeconds = 30.0f;
		// sr = new ShapeRenderer();
	}

	@Override
	public void dispose()
	{
		batch.dispose();
		b2debugRenderer.dispose();
	}

	public void resize( int width, int height )
	{
		camera.viewportWidth = (VIEWPORT_HEIGHT / height) * width;
		camera.update();
		cameraGUI.viewportHeight = VIEWPORT_GUI_HEIGHT;
		cameraGUI.viewportWidth = (VIEWPORT_GUI_HEIGHT / (float) height) * (float) width;
		cameraGUI.position.set(cameraGUI.viewportWidth / 2, cameraGUI.viewportHeight / 2, 0);
		cameraGUI.update();
		btnLeftRect.set(15, cameraGUI.viewportHeight - 15 - btnLeft.getRegionHeight(), btnLeft.getRegionWidth(), btnLeft.getRegionHeight());
		btnRightRect.set(15 + btnLeft.getRegionWidth() + 3, cameraGUI.viewportHeight - 15 - btnRight.getRegionHeight(), btnRight.getRegionWidth(),
				btnRight.getRegionHeight());
		btnJumpRect.set(cameraGUI.viewportWidth - 15 - btnJump.getRegionWidth(), cameraGUI.viewportHeight - 15 - btnJump.getRegionHeight(),
				btnJump.getRegionWidth(), btnJump.getRegionHeight());
		btnThrowRect.set(cameraGUI.viewportWidth - 15 - btnJump.getRegionWidth() - btnThrow.getRegionWidth() - 3,
				cameraGUI.viewportHeight - 15 - btnThrow.getRegionHeight(), btnThrow.getRegionWidth(), btnThrow.getRegionHeight());
		btnPauseRect.set(cameraGUI.viewportWidth - 15 - btnPause.getRegionWidth(), 15 + btnPause.getRegionHeight(), btnPause.getRegionWidth(),
				btnPause.getRegionHeight());
	}

	public void render()
	{
		gameLogic.cameraHelper.applyTo(camera);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		gameLogic.level.render(batch);
		if (GamePreferences.getInstance().drawDebug)
		{
			// // RENDER MONSTER AND GOLDCOIN LEFT
			int monsterCount = 0;
			int moneyCount = 0;
			for (Enemy e : gameLogic.level.enemies)
			{
				if (e.isAlive() && e.body.isActive())
					monsterCount++;
			}
			for (GoldCoin gc : gameLogic.level.goldCoins)
			{
				if (!gc.collected)
					moneyCount++;
			}
			if (monsterCount > 0 || moneyCount > 0)
			{
				float xx = cameraGUI.viewportWidth / 2;
				float yy = 0;
				BitmapFont fontTopOfTheScreen = MyAssetsManager.instance.fontNormal;
				fontTopOfTheScreen.drawMultiLine(batch, "Monsters: " + monsterCount + " Money: " + moneyCount, xx, yy, 0, BitmapFont.HAlignment.CENTER);
				fontTopOfTheScreen.setColor(1, 0, 0, 1);
			}
			batch.end();
			b2debugRenderer.render(gameLogic.box2dWorld, camera.combined);
		}
		if (batch.isDrawing())
		{
			batch.end();
		}
		// render GUI
		batch.setProjectionMatrix(cameraGUI.combined);
		// render buttons
		batch.begin();
		batch.draw(btnLeft, 15, cameraGUI.viewportHeight - 15 - btnLeft.getRegionHeight(), btnLeft.getRegionWidth(), btnLeft.getRegionHeight());
		batch.draw(btnRight, 15 + btnLeft.getRegionWidth() + 3, cameraGUI.viewportHeight - 15 - btnRight.getRegionHeight(), btnRight.getRegionWidth(),
				btnRight.getRegionHeight());
		batch.draw(btnJump, cameraGUI.viewportWidth - 15 - btnJump.getRegionWidth(), cameraGUI.viewportHeight - 15 - btnJump.getRegionHeight(),
				btnJump.getRegionWidth(), btnJump.getRegionHeight());
		batch.draw(btnThrow, cameraGUI.viewportWidth - 15 - btnJump.getRegionWidth() - btnThrow.getRegionWidth() - 3,
				cameraGUI.viewportHeight - 15 - btnThrow.getRegionHeight(), btnThrow.getRegionWidth(), btnThrow.getRegionHeight());
		batch.draw(btnPause, cameraGUI.viewportWidth - 15 - btnPause.getRegionWidth(), 15 + btnPause.getRegionHeight(), btnPause.getRegionWidth(),
				btnPause.getRegionHeight());
		rightButtonOnScreenIsPressed = false;
		leftButtonOnScreenIsPressed = false;
		for (int i = 0; i < 5; i++)
		{
			if (!Gdx.input.isTouched(i) || gameLogic.level.isComplete())
			{
				if (i == 0)
				{
					gameLogic.level.player.throwTime = 0.0f;
				}
				continue;
			}
			Vector3 touchPoint = new Vector3();
			cameraGUI.unproject(touchPoint.set(Gdx.input.getX(i), Gdx.input.getY(i), 0));
			if (btnLeftRect.contains(touchPoint.x, touchPoint.y) && i == 0)
			{
				leftButtonOnScreenIsPressed = true;
			}
			else
			{
				leftButtonOnScreenIsPressed = false;
				if (btnRightRect.contains(touchPoint.x, touchPoint.y) && i == 0)
				{
					rightButtonOnScreenIsPressed = true;
				}
				else
				{
					rightButtonOnScreenIsPressed = false;
					if (btnJumpRect.contains(touchPoint.x, touchPoint.y) && (i == 0 || i == 1))
					{
						gameLogic.level.player.setJumping();
					}
					else
						if (btnThrowRect.contains(touchPoint.x, touchPoint.y))
						{
							// player shot
							gameLogic.level.player.throwTime += Gdx.graphics.getDeltaTime();
						}
						else
						{
							if (btnPauseRect.contains(touchPoint.x, touchPoint.y) && Gdx.input.justTouched())
							{
								MyAssetsManager.instance.playClickSound();
								if (GameScreen.isPaused())
									GameScreen.setPaused(false);
								else
									GameScreen.setPaused(true);
							}
						}
				}
			}
		}
		// renderGuiScore
		float x = -15;
		float y = -15;
		float offsetX = 50;
		float offsetY = 50;
		// render shaking gold coin and whistle images while
		if ((gameLogic.scoreVisual < gameLogic.level.player.score || this.canDrawExplosion) && !GameScreen.isPaused() && !gameLogic.level.isComplete())
		{
			long shakeAngle = System.currentTimeMillis() % 360;
			float shakeDist = 1.5f;
			offsetX += MathUtils.sinDeg(shakeAngle * 2.2f) * shakeDist;
			offsetY += MathUtils.sinDeg(shakeAngle * 2.9f) * shakeDist;
		}
		batch.draw(MyAssetsManager.instance.textures.get("assets1/Other/coinGold"), x, y, offsetX, offsetY, 100, 100, 0.5f, -0.5f, 0);
		batch.draw(MyAssetsManager.instance.textures.get("assets1/whistle"), x, y + 35, offsetX, offsetY, 100, 100, 0.4f, -0.4f, 0);
		// draw nice score :)
		MyAssetsManager.instance.statistics40.draw(batch, String.valueOf((int) gameLogic.scoreVisual), x + 75, y + 25);
		MyAssetsManager.instance.statistics40.draw(batch, ((int) ((GameRenderer.whistleTimerInSeconds / 30f) * 100f)) + "%", x + 75, y + 60);
		// renderGuiExtraLive
		float xi = cameraGUI.viewportWidth - 50 - gameLogic.level.player.initialLives * 40;
		float yi = -15;
		for (int i = 0; i < gameLogic.level.player.lives; i++)
		{
			if (gameLogic.level.player.lives <= i)
			{
				batch.setColor(0.5f, 0.5f, 0.5f, 0.5f);
			}
			batch.draw(gameLogic.level.player.currAnimation.getKeyFrame(1), xi + i * 40, yi, 50, 50, 120, 100, 0.35f, -0.35f, 0);
			batch.setColor(1, 1, 1, 1);
		}
		if (gameLogic.level.player.lives >= 0 && gameLogic.livesVisual > gameLogic.level.player.lives)
		{
			if (GamePreferences.getInstance().sound && canPlayBlahBlahBlah)
			{
				MyAssetsManager.instance.blahblah.play(GamePreferences.getInstance().volSound);
			}
			canPlayBlahBlahBlah = false;
			int i = gameLogic.level.player.lives;
			float alphaColor = Math.max(0, gameLogic.livesVisual - gameLogic.level.player.lives - 0.5f);
			float alphaScale = 0.35f * (2 + gameLogic.level.player.lives - gameLogic.livesVisual) * 2;
			float alphaRotate = -45 * alphaColor;
			batch.setColor(1.0f, 0.7f, 0.7f, alphaColor);
			batch.draw(gameLogic.level.player.currAnimation.getKeyFrame(1), xi + i * 50, yi, 50, 50, 120, 100, alphaScale, -alphaScale, alphaRotate);
			batch.setColor(1, 1, 1, 1);
		}
		else
		{
			canPlayBlahBlahBlah = true;
		}
		// renderGuiGameOverMessage
		if (gameLogic.isGameOver())
		{
			BitmapFont fontGameOver = MyAssetsManager.instance.nice60Font;
			fontGameOver.drawMultiLine(batch, "GAME OVER\n", cameraGUI.viewportWidth / 2, cameraGUI.viewportHeight / 2, 0, BitmapFont.HAlignment.CENTER);
			fontGameOver.setColor(1, 1, 1, 1);
		}
		else
			// renderGuiGameOverMessage
			if (gameLogic.level.isComplete())
			{
				float xx = cameraGUI.viewportWidth / 2;
				float yy = cameraGUI.viewportHeight / 2;
				BitmapFont fontGameOver = MyAssetsManager.instance.nice60Font;
				fontGameOver.drawMultiLine(batch, "CONGRATULATIONS!", xx, yy, 0, BitmapFont.HAlignment.CENTER);
				fontGameOver.setColor(1, 1, 1, 1);
			}
			else
			// this is place where game isn't finished and completed
			{
				// render BOOM effect if someone whistle to microphone
				if ((GameLogic.whistleIsDetect || Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) && whistleTimerInSeconds == 30.0f)
				{
					if (gameLogic.cameraHelper.getTarget() instanceof Player)
					{
						canDrawExplosion = true;
						gameLogic.killAllMonstersFromScreen();
					}
				}
				if (canDrawExplosion && !GameScreen.isPaused())
				{
					if (whistleTimerInSeconds > 0f)
					{
						whistleTimerInSeconds -= 10 * Gdx.graphics.getDeltaTime();
					}
					else
					{
						whistleTimerInSeconds = 0f;
					}
					this.timeForExplosion += Gdx.graphics.getDeltaTime();
					
					if (timeForExplosion < 3f)
					{
						batch.setProjectionMatrix(camera.combined);
						effectShoutExplosion.setPosition(gameLogic.level.player.position.x, gameLogic.level.player.position.y);
						effectShoutExplosion.draw(batch, Gdx.graphics.getDeltaTime());
					}
					else
					{
						batch.setProjectionMatrix(cameraGUI.combined);
						effectShoutExplosion.reset();
						canDrawExplosion = false;
						timeForExplosion = 0f;
					}
				}
				else
				{
					if (!GameScreen.isPaused())
					{
						if (whistleTimerInSeconds < 30f)
						{
							whistleTimerInSeconds += Gdx.graphics.getDeltaTime();
						}
						else
						{
							whistleTimerInSeconds = 30f;
						}
					}
				}
				if (GameScreen.isPaused())
				{
					MyAssetsManager.instance.nice60Font.draw(batch, "PAUSED", (Gdx.graphics.getWidth() / 2) - 150, (Gdx.graphics.getHeight() / 2) - 40);
				}
			}
		batch.end();
		if (GamePreferences.getInstance().LogFps)
		{
			fpslog.log();
		}
	}
}
