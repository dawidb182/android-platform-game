package pl.majority.game.objects;

import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class MyOwnDialogLibgdx extends Dialog
{

	public MyOwnDialogLibgdx(String title)
	{
		super(title, MyAssetsManager.instance.skin);
		initialize();
	}

	private void initialize()
	{
		padTop(60);
		getButtonTable().defaults().height(100).width(120);
		setModal(true);
		setMovable(false);
		setResizable(false);
		
	}

	@Override
	public MyOwnDialogLibgdx text(String text)
	{
		
		super.text(new Label(text, MyAssetsManager.instance.skin, "default"));
		return this;
	}


	public MyOwnDialogLibgdx button(String buttonText, InputListener listener)
	{
		
		TextButton button = new TextButton(buttonText, MyAssetsManager.instance.skin);
		button.sizeBy(120, 100);
		button.addListener(listener);
		button(button);
		return this;
	}

	@Override
	public float getPrefWidth()
	{
		return 300f;
	}

	@Override
	public float getPrefHeight()
	{
		return 200f;
	}
	

}
