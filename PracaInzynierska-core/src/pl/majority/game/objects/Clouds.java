package pl.majority.game.objects;

import pl.majority.game.GameRenderer;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Clouds extends AbstractGameObject
{
	private float					length;
	private Array<TextureRegion>	regClouds;
	private Array<Cloud>			clouds;

	private class Cloud extends AbstractGameObject
	{
		private TextureRegion	regCloud;

		public Cloud()
		{
		}

		public void setRegion(TextureRegion region)
		{
			regCloud = region;
		}

		@Override
		public void render(SpriteBatch batch)
		{
			TextureRegion reg = regCloud;
			
			batch.draw(reg.getTexture(), position.x + origin.x, position.y + origin.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, reg.getRegionX(), reg.getRegionY(),
					reg.getRegionWidth(), reg.getRegionHeight(), false, false);
		}

	}

	public Clouds( float length )
	{
		this.length = length;

		// dimension.set();
		regClouds = new Array<TextureRegion>();
		regClouds.add(MyAssetsManager.instance.textures.get("assets1/Items/cloud1"));
		regClouds.add(MyAssetsManager.instance.textures.get("assets1/Items/cloud2"));
		regClouds.add(MyAssetsManager.instance.textures.get("assets1/Items/cloud3"));

		int distFac = 5;
		int numClouds = (int) (length / distFac);
		clouds = new Array<Cloud>(2 * numClouds);
		for (int i = 0; i < numClouds; i++)
		{
			Cloud cloud = spawnCloud();
			cloud.dimension.set(MathUtils.random(1f,2f), MathUtils.random(0.3f,0.7f));
			cloud.position.x = i * distFac;
			cloud.position.y = GameRenderer.VIEWPORT_HEIGHT - this.dimension.y + MathUtils.random(-0.5f, 0.5f);
			cloud.velocity.x = MathUtils.random(0.5f, 2.0f); 
			
			clouds.add(cloud);
		}
	}

	private Cloud spawnCloud()
	{
		Cloud cloud = new Cloud();

		// select random cloud image
		cloud.setRegion(regClouds.random());
		// position
		Vector2 pos = new Vector2();
		pos.x = length + 10; // position after end of level
		pos.y += 1.75; // base position
		// random additional position
		pos.y += MathUtils.random(0.0f, 0.2f) * (MathUtils.randomBoolean() ? 1 : -1);
		cloud.position.set(pos);
		return cloud;
	}

	@Override
	public void update(float deltaTime)
	{
		for(Cloud c : clouds)
		{
			if(c.position.x > 50)
				c.position.x = -c.dimension.x;
			
			c.position.x += c.velocity.x * deltaTime;
		}
	}

	@Override
	public void render(SpriteBatch batch)
	{
		for (Cloud cloud : clouds)
			cloud.render(batch);
	}
}
