package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Flag extends AbstractGameObject
{
	public FlagType			flagType;
	public Animation		animation;
	public boolean			isWaving;
	private float			stateTime;

	private TextureRegion	currFrame;

	public enum FlagType
	{
		FLAGYELLOW, FLAGRED, FLAGGREEN, FLAGBLUE
	}

	public Flag( FlagType flagtype , int x , int y )
	{
		stateTime = 0.0f;
		this.position.x = x;
		this.position.y = y;
		dimension.set(1, 1);
		this.isWaving = true;
		currFrame = new TextureRegion();
		this.flagType = flagtype;
		TextureRegion[] textureRegions;
		switch (flagtype)
		{
			case FLAGBLUE :

				TextureRegion flagBlue = MyAssetsManager.instance.textures
						.get("assets1/Items/flagBlue");

				TextureRegion flagBlue2 = MyAssetsManager.instance.textures
						.get("assets1/Items/flagBlue2");

				TextureRegion flagBlueHanging = MyAssetsManager.instance.textures
						.get("assets1/Items/flagBlueHanging");

				textureRegions = new TextureRegion[]{flagBlueHanging, flagBlue,
						flagBlue2};

				animation = new Animation(0.250f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP);

				break;

			case FLAGGREEN :

				TextureRegion flagGreen = MyAssetsManager.instance.textures
						.get("assets1/Items/flagGreen");

				TextureRegion flagGreen2 = MyAssetsManager.instance.textures
						.get("assets1/Items/flagGreen2");

				TextureRegion flagGreenHanging = MyAssetsManager.instance.textures
						.get("assets1/Items/flagGreenHanging");

				textureRegions = new TextureRegion[]{flagGreenHanging,
						flagGreen, flagGreen2};

				animation = new Animation(0.250f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP);

				break;

			case FLAGRED :

				TextureRegion flagRed = MyAssetsManager.instance.textures
						.get("assets1/Items/flagRed");

				TextureRegion flagRed2 = MyAssetsManager.instance.textures
						.get("assets1/Items/flagRed2");

				TextureRegion flagRedHanging = MyAssetsManager.instance.textures
						.get("assets1/Items/flagRedHanging");

				textureRegions = new TextureRegion[]{flagRedHanging, flagRed,
						flagRed2};

				animation = new Animation(0.250f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP);

				break;

			case FLAGYELLOW :

				TextureRegion flagYellow = MyAssetsManager.instance.textures
						.get("assets1/Items/flagYellow");

				TextureRegion flagYellow2 = MyAssetsManager.instance.textures
						.get("assets1/Items/flagYellow2");

				TextureRegion flagYellowHanging = MyAssetsManager.instance.textures
						.get("assets1/Items/flagYellowHanging");

				textureRegions = new TextureRegion[]{flagYellowHanging,
						flagYellow2, flagYellow};

				animation = new Animation(0.250f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP);

				break;

			default :
				break;
		}
	}

	@Override
	public void render(SpriteBatch batch)
	{
		batch.draw(currFrame, position.x, position.y, dimension.x, dimension.y);
	}

	@Override
	public void update(float deltaTime)
	{
		if (this.isWaving)
		{
			stateTime += deltaTime;
			currFrame = animation.getKeyFrame(stateTime, true);
		}
		else
		{
			currFrame = animation.getKeyFrame(0);
		}
	}

}
