
package pl.majority.game.objects;

import java.util.HashMap;
import java.util.Map;

import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.AudioRecorder;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Disposable;

public class MyAssetsManager implements Disposable , AssetErrorListener
{
	public static final MyAssetsManager instance = new MyAssetsManager();
	private AssetManager assetManager;
	// paths
	private final String atlasPath = "images/majority.atlas";
	// main structure
	public Map<String, AtlasRegion> textures = new HashMap<String, TextureAtlas.AtlasRegion>();
	// defaults fonts
	public BitmapFont fontNormal, nice60Font, statistics40;
	public Skin skin;
	public boolean drawDebug = false;
	public boolean LogFps = false;
	// AUDIO
	public Sound coin, throwing, throwingInTrampoline, applause, alert1, alert2, blahblah, click, dead, fireworks, powerUp, strangeMusic;// , jumping;
	public Music gameMusicScreen, mainScreenMusic;
	public AudioRecorder audio;

	public void init( AssetManager assetManager )
	{
		audio = Gdx.audio.newAudioRecorder(44100, true);
		this.assetManager = assetManager;
		assetManager.setErrorListener(this);
		assetManager.load(atlasPath, TextureAtlas.class);
		// audio load
		assetManager.load("audio/coin.wav", Sound.class);
		assetManager.load("audio/alert1.wav", Sound.class);
		assetManager.load("audio/alert2.wav", Sound.class);
		assetManager.load("audio/blah-blah-blah.wav", Sound.class);
		assetManager.load("audio/click.wav", Sound.class);
		assetManager.load("audio/dead.wav", Sound.class);
		assetManager.load("audio/fireworks.wav", Sound.class);
		assetManager.load("audio/gameScreenMusic.mp3", Music.class);
		assetManager.load("audio/MocnyBansoTrans.mp3", Music.class);
		assetManager.load("audio/powerUp.wav", Sound.class);
		assetManager.load("audio/strangeMusic.wav", Sound.class);
		assetManager.load("audio/applause.wav", Sound.class);
		assetManager.load("audio/sfx_throw.wav", Sound.class);
		assetManager.load("audio/cartoon-throw.wav", Sound.class);
		assetManager.finishLoading();
		// apply linear filtering for every atlas and create arrays ofs
		// AtlasRegions
		TextureAtlas atlas = assetManager.get(atlasPath);
		for (AtlasRegion reg : atlas.getRegions())
		{
			reg.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			textures.put(reg.name, reg);
		}
		// INIT FONTS
		fontNormal = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), false);
		nice60Font = new BitmapFont(Gdx.files.internal("images/60fnt.fnt"), true);
		statistics40 = new BitmapFont(Gdx.files.internal("images/40Statistics.fnt"), true);
		fontNormal.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		nice60Font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		statistics40.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		skin = new Skin(Gdx.files.internal("fonts/jsons/uiSKIN.json"), new TextureAtlas(Gdx.files.internal("images/majority.atlas")));
		// audio
		coin = assetManager.get("audio/coin.wav");
		alert1 = assetManager.get("audio/alert1.wav");
		alert2 = assetManager.get("audio/alert2.wav");
		blahblah = assetManager.get("audio/blah-blah-blah.wav");
		click = assetManager.get("audio/click.wav");
		dead = assetManager.get("audio/dead.wav");
		fireworks = assetManager.get("audio/fireworks.wav");
		powerUp = assetManager.get("audio/powerUp.wav");
		strangeMusic = assetManager.get("audio/strangeMusic.wav");
		gameMusicScreen = assetManager.get("audio/gameScreenMusic.mp3");
		mainScreenMusic = assetManager.get("audio/MocnyBansoTrans.mp3");
		applause = assetManager.get("audio/applause.wav");
		throwing = assetManager.get("audio/sfx_throw.wav");
		throwingInTrampoline = assetManager.get("audio/cartoon-throw.wav");
	}

	@Override
	public void dispose()
	{
		skin.dispose();
		fontNormal.dispose();
		coin.dispose();
		alert1.dispose();
		alert2.dispose();
		blahblah.dispose();
		click.dispose();
		dead.dispose();
		fireworks.dispose();
		powerUp.dispose();
		strangeMusic.dispose();
		throwingInTrampoline.dispose();
		gameMusicScreen.dispose();
		mainScreenMusic.dispose();
		audio.dispose();
		assetManager.dispose();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void error( AssetDescriptor asset, Throwable throwable )
	{
		Gdx.app.error(MyAssetsManager.class.getName(), "Couldn't load asset '" + asset.fileName + "'", throwable);
	}

	public void playClickSound()
	{
		if (GamePreferences.getInstance().sound)
		{
			MyAssetsManager.instance.click.play(GamePreferences.getInstance().volSound);
		}
	}
}
