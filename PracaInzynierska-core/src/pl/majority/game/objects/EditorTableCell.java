package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

public class EditorTableCell extends Image
{
	public Array<Image>	additionals;
	public boolean		isPlatformAdditional;

	
	public EditorTableCell( TextureRegion tr )
	{
		super(tr);
		additionals = new Array<Image>();
		isPlatformAdditional = false;
	}

	public EditorTableCell()
	{
		super();
		additionals = new Array<Image>();
		isPlatformAdditional = false;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		if (isPlatformAdditional)
		{
			additionals.get(0).setPosition(super.getX() , super.getY());
			additionals.get(1).setPosition(super.getX() + super.getWidth() /2, super.getY());
			
			additionals.get(0).draw(batch, parentAlpha);
			additionals.get(1).draw(batch, parentAlpha);
		}
		else
		{
			for (Image etc : additionals)
			{
				etc.setPosition(super.getX(), super.getY());
				etc.draw(batch, parentAlpha);
			}

		}
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);

		for (Image etc : additionals)
		{
			etc.act(delta);
		}

	}
	
	public boolean isPlatformAdditional()
	{
		return isPlatformAdditional;
	}

	public void setPlatformAdditional(boolean isPlatformAdditional)
	{
		this.isPlatformAdditional = isPlatformAdditional;
	}


}
