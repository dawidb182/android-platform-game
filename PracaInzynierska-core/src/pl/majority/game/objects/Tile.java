
package pl.majority.game.objects;

import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Tile extends AbstractGameObject
{
	private TextureRegion reg;
	public String texPath;

	public Tile( String name, int posX, int posY )
	{
		this.position.x = posX;
		this.position.y = posY;
		this.dimension.set(1.01f, 1.01f);
		this.bounds.set(position.x, position.y, dimension.x, dimension.y);
		// get full texture name
		texPath = searchFullPathInMap(name);
		reg = MyAssetsManager.instance.textures.get(texPath);
		if (reg == null)
		{
			Gdx.app.error(Tile.class.getName(), "Cannot find texture from main MAP with given: \"" + name + "\" name");
		}
	}

	@Override
	public void render( SpriteBatch batch )
	{
		batch.draw(reg.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, reg.getRegionX(),
				reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
	}

	private String searchFullPathInMap( String inputString )
	{
		String minString = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"; // simple
		// example
		// :)
		for (Entry<String, AtlasRegion> entry : MyAssetsManager.instance.textures.entrySet())
		{
			if (entry.getKey().toUpperCase().contains(inputString.toUpperCase()))
			{
				if (entry.getKey().length() < minString.length())
				{
					minString = entry.getKey();
				}
			}
		}
		if (inputString.compareTo(minString) != 0)
			return minString;
		else
			return inputString;
	}
}
