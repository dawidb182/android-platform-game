package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Torch extends AbstractGameObject
{
	public Animation	animation;
	public float delta;
	
	public Torch(int x, int y)
	{
		this.delta = 0;
		this.position.x = x;
		this.position.y = y;
		
		TextureRegion torch1 = MyAssetsManager.instance.textures
				.get("assets1/Tiles/tochLit");
		TextureRegion torch2 = MyAssetsManager.instance.textures
				.get("assets1/Tiles/tochLit2");

		TextureRegion[] textureRegions = {torch1, torch2};

		animation = new Animation(0.250f, textureRegions);
		animation.setPlayMode(PlayMode.LOOP);

	}

	@Override
	public void render(SpriteBatch batch)
	{
		batch.draw(animation.getKeyFrame(delta), position.x, position.y);
	}

	@Override
	public void update(float deltaTime)
	{
		delta += deltaTime;
	}

}
