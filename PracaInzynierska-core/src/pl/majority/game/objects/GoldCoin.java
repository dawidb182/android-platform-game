package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GoldCoin extends AbstractGameObject
{
	private TextureRegion	regGoldCoin;
	private GoldCoinType	goldCoinType;
	public boolean			collected;

	public enum GoldCoinType
	{
		GOLD, SILVER, BRONZE
	}

	public GoldCoin( GoldCoinType type , float x , float y )
	{
		this.position.x = x;
		this.position.y = y;

		this.goldCoinType = type;
		switch (type)
		{
			case SILVER :
				regGoldCoin = MyAssetsManager.instance.textures
						.get("assets1/Other/coinSilver");
				break;
			case BRONZE :
				regGoldCoin = MyAssetsManager.instance.textures
						.get("assets1/Other/coinBronze");
				break;
			case GOLD :
				regGoldCoin = MyAssetsManager.instance.textures
						.get("assets1/Other/coinGold");
				break;
		}
		
		bounds.set(0, 0, dimension.x, dimension.y);
		collected = false;
	}

	public void render(SpriteBatch batch)
	{
		if (collected)
		{
			this.body.setActive(false);

			return;
		}

		TextureRegion reg = null;
		reg = regGoldCoin;
		batch.draw(reg.getTexture(), position.x, position.y, origin.x,
				origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation,
				reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(),
				reg.getRegionHeight(), false, false);
	}

	@Override
	public void update(float delta)
	{
		if (this.collected)
		{
			this.body.setActive(false);
			return;
		}
	}

	public int getScore()
	{
		if (this.goldCoinType == GoldCoinType.BRONZE)
			return 10;
		else
			if (this.goldCoinType == GoldCoinType.SILVER)
				return 20;
			else
				if (this.goldCoinType == GoldCoinType.GOLD)
					return 50;

		return -1500100900;
	}
}
