
package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class MovingPlatform extends AbstractGameObject
{
	private TextureRegion platformTextureRegion;
	private Vector2 startPoint, endPoint, current, next, temp;
	private Array<Vector2> platformTrace;
	private int index;
	private int deltaMove;
	private Player playerReference;

	public MovingPlatform( String name, Array<Vector2> trace )
	{
		deltaMove = 1;
		index = 1;
		platformTrace = trace;
		dimension.set(1.0f, 1.0f);
		position.set(platformTrace.get(0).x, platformTrace.get(0).y);
		bounds.set(position.x, position.y, dimension.x, dimension.y);
		temp = new Vector2();
		platformTextureRegion = MyAssetsManager.instance.textures.get(name);
		startPoint = new Vector2(position);
		endPoint = new Vector2(platformTrace.get(platformTrace.size - 1).x, platformTrace.get(platformTrace.size - 1).y);
		current = new Vector2(startPoint);
		next = new Vector2(platformTrace.get(1));
		velocity.set(next.cpy().sub(current));
	}

	public void render( SpriteBatch batch )
	{
		TextureRegion reg = platformTextureRegion;
		batch.draw(reg.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, reg.getRegionX(),
				reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
	}

	@Override
	public void update( float deltaTime )
	{
		position = body.getPosition();
		float speed = 1f;
		Vector2 direction = temp.set(next).sub(position);
		float distanceToTarget = direction.len();
		float travelDistance = speed * deltaTime;
		// the target is very close, so we set the position to the target directly
		if (distanceToTarget <= travelDistance)
		{
			body.setTransform(next, 0);
			if (getPlayerReference() != null)
			{
				Vector2 playerPosition = getPlayerReference().body.getPosition();
				getPlayerReference().body.setTransform(playerPosition.x + (next.x - position.x), playerPosition.y + (next.y - position.y), 0);
			}
			// apply velocity to player
			if (next.equals(startPoint) || next.equals(endPoint))
			{
				deltaMove *= -1;
			}
			current = next;
			index = index + deltaMove;
			next = platformTrace.get(index);
		}
		else
		{
			direction.nor();
			// move a bit in the target direction
			velocity.set(direction);
			body.setLinearVelocity(velocity);
		}
		// apply velocity to player
		if (getPlayerReference() != null)
		{
			getPlayerReference().body.setLinearVelocity(velocity);
		}
	}

	public Player getPlayerReference()
	{
		return playerReference;
	}

	public void setPlayerReference( Player playerReference )
	{
		this.playerReference = playerReference;
	}
}
