
package pl.majority.game.objects;

import java.io.IOException;

import pl.majority.game.GameRenderer;
import pl.majority.game.objects.Enemy.ENEMY_TYPE;
import pl.majority.game.objects.Flag.FlagType;
import pl.majority.game.objects.GoldCoin.GoldCoinType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

public class Level
{
	// main board
	public Array<Tile> tilesBoard;
	public Array<Tile> tilesAdditional;
	public Array<Torch> torches;
	public Array<Flag> flags;
	public Array<MovingPlatform> movingPlatformsArray;
	public Array<JumpingTrampoline> jumpingTrampolines;
	public FinalPoint finalPoint;
	public boolean isFinished;
	// decoration
	private boolean cloudsIsVisible;
	public Clouds clouds;
	public Player player;
	public Array<GoldCoin> goldCoins;
	public Array<Enemy> enemies;

	public Level( String levelPath )
	{
		clouds = new Clouds(GameRenderer.VIEWPORT_WIDTH);
		isFinished = false;
		torches = new Array<Torch>();
		flags = new Array<Flag>();
		tilesBoard = new Array<Tile>();
		tilesAdditional = new Array<Tile>();
		goldCoins = new Array<GoldCoin>();
		enemies = new Array<Enemy>();
		jumpingTrampolines = new Array<JumpingTrampoline>();
		movingPlatformsArray = new Array<MovingPlatform>();
		loadMapFromXMLFile(levelPath);
	}

	public void render( SpriteBatch batch )
	{
		if (cloudsIsVisible)
		{
			clouds.render(batch);
		}
		finalPoint.render(batch);
		for (Tile t : tilesBoard)
		{
			t.render(batch);
		}
		for (Tile t : tilesAdditional)
		{
			t.render(batch);
		}
		for (Torch t : torches)
		{
			t.render(batch);
		}
		for (Flag f : flags)
		{
			f.render(batch);
		}
		for (GoldCoin gc : goldCoins)
		{
			gc.render(batch);
		}

		for (MovingPlatform mp : movingPlatformsArray)
		{
			mp.render(batch);
		}
		for (JumpingTrampoline jt : jumpingTrampolines)
		{
			jt.render(batch);
		}
		for (Enemy e : enemies)
		{
			e.render(batch);
		}
		player.render(batch);
	}

	public void update( float deltaTime )
	{
		for (Torch t : torches)
		{
			t.update(deltaTime);
		}
		for (GoldCoin gc : goldCoins)
		{
			gc.update(deltaTime);
		}
		for (Enemy e : enemies)
		{
			e.update(deltaTime);
			if (!e.body.isActive())
			{
				enemies.removeValue(e, true);
			}
		}
		for (Flag f : flags)
		{
			f.update(deltaTime);
		}
		for (MovingPlatform mp : movingPlatformsArray)
		{
			mp.update(deltaTime);
		}
		for (JumpingTrampoline jt : jumpingTrampolines)
		{
			jt.update(deltaTime);
		}
		if (cloudsIsVisible)
		{
			clouds.update(deltaTime);
		}
		player.update(deltaTime);
	}

	private void loadMapFromXMLFile( String xmlName )
	{
		XmlReader reader = new XmlReader();
		try
		{
			Element root = reader.parse(Gdx.files.local("./bin/levels/" + xmlName));
			Element tiles = root.getChildByName("tiles");
			Element additionals = root.getChildByName("additionals");
			Element bonuses = root.getChildByName("bonuses");
			Element enemies = root.getChildByName("enemies");
			Element movingPlatforms = root.getChildByName("platforms");
			Element playerXML = root.getChildByName("player");
			Element finalPointEl = root.getChildByName("final");
			Element jumpingPlatforms = root.getChildByName("jumpingPlatforms");
			this.cloudsIsVisible = root.getChildByName("clouds").getBoolean("visible");
			Array<Element> xmlTile = tiles.getChildrenByName("tile");
			Array<Element> xmlAdditionals = additionals.getChildrenByName("additional");
			Array<Element> xmlBonuses = bonuses.getChildrenByName("bonus");
			Array<Element> xmlEnemies = enemies.getChildrenByName("enemy");
			Array<Element> xmlMovingPlatforms = movingPlatforms.getChildrenByName("platform");
			Array<Element> xmlJumpingPlatforms = jumpingPlatforms.getChildrenByName("platform");
			// ruszajace sie platformy
			for (Element platform : xmlMovingPlatforms)
			{
				String name = platform.getAttribute("name");
				Array<Vector2> trace = new Array<Vector2>();
				for (Element platformTracePoint : platform.getChildrenByName("point"))
				{
					int x = platformTracePoint.getInt("x");
					int y = platformTracePoint.getInt("y");
					trace.add(new Vector2(x, y));
				}
				movingPlatformsArray.add(new MovingPlatform(name, trace));
			}
			// glowne kafelki ktore nie ruszaja sie
			for (Element tile : xmlTile)
			{
				String name = tile.getAttribute("name");
				int posX = tile.getInt("posX");
				int posY = tile.getInt("posY");
				tilesBoard.add(new Tile(name, posX, posY));
			}
			// glowne dodatki ktore nie wchodza w interakcje z użytkownikiem
			for (Element additional : xmlAdditionals)
			{
				String name = additional.getAttribute("name");
				int posX = additional.getInt("posX");
				int posY = additional.getInt("posY");
				switch (name.toUpperCase())
				{
					case "TORCH" :
						torches.add(new Torch(posX, posY));
						break;
					case "FLAGRED" :
						flags.add(new Flag(FlagType.FLAGRED, posX, posY));
						break;
					case "FLAGGREEN" :
						flags.add(new Flag(FlagType.FLAGGREEN, posX, posY));
						break;
					case "FLAGBLUE" :
						flags.add(new Flag(FlagType.FLAGBLUE, posX, posY));
						break;
					case "FLAGYELLOW" :
						flags.add(new Flag(FlagType.FLAGYELLOW, posX, posY));
						break;
					default :
						tilesAdditional.add(new Tile(name, posX, posY));
						break;
				}
			}
			for (Element bonus : xmlBonuses)
			{
				String name = bonus.getAttribute("name");
				int posX = bonus.getInt("posX");
				int posY = bonus.getInt("posY");
				switch (name.toUpperCase())
				{
					case "COINGOLD" :
						goldCoins.add(new GoldCoin(GoldCoinType.GOLD, posX, posY));
						break;
					case "COINSILVER" :
						goldCoins.add(new GoldCoin(GoldCoinType.SILVER, posX, posY));
						break;
					case "COINBRONZE" :
						goldCoins.add(new GoldCoin(GoldCoinType.BRONZE, posX, posY));
						break;
					default :
						break;
				}
			}
			for (Element enemy : xmlEnemies)
			{
				// BLOCKER, FISH, FLY, SLIME, SNAIL
				String name = enemy.getAttribute("name");
				int posX = enemy.getInt("posX");
				int posY = enemy.getInt("posY");
				switch (name.toUpperCase())
				{
					case "BLOCKER" :
						this.enemies.add(new Enemy(ENEMY_TYPE.BLOCKER, posX, posY));
						break;
					case "FISH" :
						this.enemies.add(new Enemy(ENEMY_TYPE.FISH, posX, posY));
						break;
					case "FLY" :
						this.enemies.add(new Enemy(ENEMY_TYPE.FLY, posX, posY));
						break;
					case "SLIME" :
						this.enemies.add(new Enemy(ENEMY_TYPE.SLIME, posX, posY));
						break;
					case "SNAIL" :
						this.enemies.add(new Enemy(ENEMY_TYPE.SNAIL, posX, posY));
						break;
					default :
						break;
				}
			}
			// glowne kafelki ktore nie ruszaja sie
			for (Element tile : xmlJumpingPlatforms)
			{
				// String name = tile.getAttribute("name");
				int posX = tile.getInt("posX");
				int posY = tile.getInt("posY");
				jumpingTrampolines.add(new JumpingTrampoline(posX, posY));
			}
			// init finish point
			int posXFinalPoint = finalPointEl.getInt("posX");
			int posYFinalPoint = finalPointEl.getInt("posY");
			finalPoint = new FinalPoint(posXFinalPoint, posYFinalPoint);
			// initialize player
			int playerPosX = playerXML.getInt("posX");
			int playerPosY = playerXML.getInt("posY");
			this.player = new Player(playerPosX, playerPosY);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public boolean isComplete()
	{
		if (isLevelEmpty() && this.isFinished)
		{
			return true;
		}
		return false;
	}

	public boolean isLevelEmpty()
	{
		for (GoldCoin gd : this.goldCoins)
		{
			if (!gd.collected)
			{
				return false;
			}
		}
		for (Enemy e : this.enemies)
		{
			if (e.isAlive())
			{
				return false;
			}
		}
		return true;
	}
}
