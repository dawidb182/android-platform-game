
package pl.majority.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Enemy extends AbstractGameObject
{
	public int hp;
	public ENEMY_LOOKING enemyIsLooking;
	public ENEMY_TYPE enemyType;
	public EnemyPosition enemyPositionType;
	// different animations and texture
	private Animation animation;
	private TextureRegion currentFrame;
	private float stateTime;
	// 5 seconds to show dead enemy
	private float deadTime = 5.0f;

	public enum EnemyPosition
	{
		FLYING, WALKING, FALLING;
	}

	public enum ENEMY_LOOKING
	{
		LEFT, RIGHT
	}

	public enum ENEMY_TYPE
	{
		BLOCKER, FISH, FLY, SLIME, SNAIL // SNAIL = �LIMAK
	}

	public Enemy( ENEMY_TYPE type, int x, int y )
	{
		this.position.x = x;
		this.position.y = y;
		this.enemyPositionType = EnemyPosition.WALKING;
		this.enemyType = type;
		currentFrame = new TextureRegion();
		loadAndSetCharacterType(type);
		initEnemyValues();
		switch (type)
		{
			case BLOCKER :
				this.hp = MathUtils.random(1, 50);
				maxVelocityWalking.set(0f, 0f);
				break;
			case FISH :
				this.hp = MathUtils.random(1, 50);
				maxVelocityWalking.set(0.5f, 1.0f);
				break;
			case SLIME :
				this.hp = MathUtils.random(1, 50);
				maxVelocityWalking.set(1f, 1.0f);
				break;
			case FLY :
				this.hp = MathUtils.random(1, 50);
				maxVelocityWalking.set(0.2f, 3.0f);
				break;
			case SNAIL :
				this.hp = MathUtils.random(1, 50);
				maxVelocityWalking.set(0.6f, 1.0f);
				break;
		}
	}

	private void initEnemyValues()
	{
		dimension.set(1, 1);
		origin.set(dimension.x / 2, dimension.y / 2);
		bounds.set(position.x, position.y, dimension.x, dimension.y);
		velocity.set(0, 0);
		// View direction
		enemyIsLooking = ENEMY_LOOKING.LEFT;
	}

	@Override
	public void render( SpriteBatch batch )
	{
		if (this.body.isActive())
		{
			batch.draw(currentFrame.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation,
					currentFrame.getRegionX(), currentFrame.getRegionY(), currentFrame.getRegionWidth(), currentFrame.getRegionHeight(),
					enemyIsLooking != ENEMY_LOOKING.LEFT, false);
		}
	}

	@Override
	public void update( float deltaTime )
	{
		if (this.isAlive())
		{
			position.set(body.getPosition());
			position.x = Math.max(-1f, position.x);
			position.x = Math.min(50f, position.x);
			position.y = Math.max(-2f, position.y);
			position.y = Math.min(20f, position.y);
			bounds.set(position.x, position.y, dimension.x, dimension.y);
			velocity.set(this.body.getLinearVelocity());
			body.setTransform(position.x, position.y, 0);
			if (this.body.isActive())
			{
				stateTime += deltaTime;
				currentFrame = animation.getKeyFrame(animation.getFrameDuration() + stateTime
						% (animation.getFrameDuration() * 3 - animation.getFrameDuration()));
			}
			switch (enemyType)
			{
				case BLOCKER :
					break;
				case FISH :
					if (enemyPositionType == EnemyPosition.WALKING)
					{
						if (Math.abs(velocity.x) < maxVelocityWalking.x)
						{
							float randomForce = MathUtils.random(0.0f, 0.2f);
							body.applyLinearImpulse(enemyIsLooking == ENEMY_LOOKING.LEFT ? -randomForce : randomForce, 0f, position.x, position.y, true);
						}
					}
					else
						if (enemyPositionType == EnemyPosition.FALLING && !this.isAlive())
						{
							body.setActive(false);
						}
					break;
				case FLY :
					body.setLinearVelocity(enemyIsLooking == ENEMY_LOOKING.LEFT ? -maxVelocityWalking.x : maxVelocityWalking.x,
							MathUtils.sinDeg(stateTime) * 0.5f);
					break;
				case SLIME :
					if (enemyPositionType == EnemyPosition.WALKING)
					{
						if (Math.abs(velocity.x) < maxVelocityWalking.x)
						{
							float randomForce = MathUtils.random(0.0f, 0.2f);
							body.applyLinearImpulse(enemyIsLooking == ENEMY_LOOKING.LEFT ? -randomForce : randomForce, 0f, position.x, position.y, true);
						}
					}
					else
						if (enemyPositionType == EnemyPosition.FALLING && !this.isAlive())
						{
							body.setActive(false);
						}
					break;
				case SNAIL :
					if (enemyPositionType == EnemyPosition.WALKING)
					{
						if (Math.abs(velocity.x) < maxVelocityWalking.x)
						{
							float randomForce = MathUtils.random(0.0f, 0.2f);
							body.applyLinearImpulse(enemyIsLooking == ENEMY_LOOKING.LEFT ? -randomForce : randomForce, 0f, position.x, position.y, true);
						}
					}
					else
						if (enemyPositionType == EnemyPosition.FALLING && !this.isAlive())
						{
							body.setActive(false);
						}
					break;
				default :
					break;
			}
		}
		else
		{
			setEnemyAsDead();
		}
	}

	public void setEnemyAsDead()
	{
		currentFrame = animation.getKeyFrame(0, false);
		this.hp = 0;
		this.body.setType(BodyType.StaticBody);
		this.body.setLinearVelocity(0f, 0f);
		this.body.setLinearDamping(0f);
		deadTime -= Gdx.graphics.getDeltaTime();
		if (deadTime <= 0)
		{
			this.body.setActive(false);
		}
	}

	// wczytuje ustawienie typu charakteru i przypisuje odpowiednia texture
	public void loadAndSetCharacterType( ENEMY_TYPE type )
	{
		TextureRegion[] textureRegions;
		switch (type)
		{
			case BLOCKER :
				TextureRegion blockerMad = MyAssetsManager.instance.textures.get("assets1/Enemies/blockerMad");
				TextureRegion blockerSad = MyAssetsManager.instance.textures.get("assets1/Enemies/blockerSad");
				TextureRegion blockerBody = MyAssetsManager.instance.textures.get("assets1/Enemies/blockerBody");
				textureRegions = new TextureRegion[]
				{blockerBody, blockerMad, blockerSad};
				animation = new Animation(0.125f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP_RANDOM);
				break;
			case FISH :
				TextureRegion fishDead = MyAssetsManager.instance.textures.get("assets1/Enemies/fishDead");
				TextureRegion fishSwim1 = MyAssetsManager.instance.textures.get("assets1/Enemies/fishSwim1");
				TextureRegion fishSwim2 = MyAssetsManager.instance.textures.get("assets1/Enemies/fishSwim2");
				textureRegions = new TextureRegion[]
				{fishDead, fishSwim1, fishSwim2};
				animation = new Animation(0.125f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP_PINGPONG);
				break;
			case FLY :
				TextureRegion flyDead = MyAssetsManager.instance.textures.get("assets1/Enemies/flyDead");
				TextureRegion flyFly1 = MyAssetsManager.instance.textures.get("assets1/Enemies/flyFly1");
				TextureRegion flyFly2 = MyAssetsManager.instance.textures.get("assets1/Enemies/flyFly2");
				textureRegions = new TextureRegion[]
				{flyDead, flyFly1, flyFly2};
				animation = new Animation(0.125f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP_PINGPONG);
				break;
			case SLIME :
				TextureRegion slimeDead = MyAssetsManager.instance.textures.get("assets1/Enemies/slimeDead");
				TextureRegion slimeWalk1 = MyAssetsManager.instance.textures.get("assets1/Enemies/slimeWalk1");
				TextureRegion slimeWalk2 = MyAssetsManager.instance.textures.get("assets1/Enemies/slimeWalk2");
				textureRegions = new TextureRegion[]
				{slimeDead, slimeWalk1, slimeWalk2};
				animation = new Animation(0.125f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP_PINGPONG);
				break;
			case SNAIL :
				TextureRegion snailShell_upsidedown = MyAssetsManager.instance.textures.get("assets1/Enemies/snailShell_upsidedown");
				TextureRegion snailWalk1 = MyAssetsManager.instance.textures.get("assets1/Enemies/snailWalk1");
				TextureRegion snailWalk2 = MyAssetsManager.instance.textures.get("assets1/Enemies/snailWalk2");
				textureRegions = new TextureRegion[]
				{snailShell_upsidedown, snailWalk1, snailWalk2};
				animation = new Animation(0.125f, textureRegions);
				animation.setPlayMode(PlayMode.LOOP_PINGPONG);
				break;
			default :
				break;
		}
	}

	public boolean isAlive()
	{	
		return (this.hp > 0 && !(this.position.x <=-1 || this.position.x >= 50 || this.position.y <= -2f || this.position.y > 21f));
	}
}
