
package pl.majority.game.objects;

import pl.majority.game.GameRenderer;
import pl.majority.game.util.GamePreferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Player extends AbstractGameObject
{
	public int lives;
	public int initialLives;
	public Array<FireBall> fireBalls;
	private final float THROW_TIME_MAX = 0.421f;
	public float throwTime;
	public int indexCurrentFireBall;
	public Vector2 initialPosition;
	public PLAYER_IS_LOOKING playerIsLooking;
	public PLAYER_STATE playerState;
	public float stateTime;
	private TextureRegion gamerAtlasRegion;
	public static final int rowsInSpritesheet = 8;
	public static final int colsInSpritesheet = 9;
	// different animations and texture
	public Animation currAnimation;
	// private Animation runningAnimation;
	private Animation throwingAnimation;
	private Animation dyingAnimation;
	private Animation walkingAnimation;
	private Animation goingUpstairsAnimation;
	private Animation goingDownstairsAnimation;
	private Animation standingAnimation;
	private Animation jumpRisingAnimation;
	private Animation jumpFallingAnimation;
	private MovingPlatform platformReference;
	// -2 - w lewo do g�ry
	// -1 - w lewo w d�
	// 0 - stoi
	// 1 - w prawo w d�
	// 2 - w prawo do g�ry
	public int goingStairs;
	public float score;

	public enum PLAYER_IS_LOOKING
	{
		LEFT, RIGHT
	}

	public enum PLAYER_STATE
	{
		STANDING, WALKING, THROWING, JUMP_RISING, JUMP_FALLING, GOING_UPSTAIRS, GOING_DOWNSTAIRS,
	}

	public Player( int posX, int posY )
	{
		this.lives = 3;
		this.initialLives = lives;
		this.position.x = posX;
		this.position.y = posY;
		score = 0;
		initialPosition = new Vector2(posX, posY);
		goingStairs = 0;
		throwTime = 0.0f;
		indexCurrentFireBall = 0;
		// init fireballs
		fireBalls = new Array<FireBall>(20);
		for (int i = 0; i < 20; i++)
		{
			fireBalls.add(new FireBall());
		}
		loadAndSetCharacterType();
		splitSpritesheetAndCreateAnimations();
		initCharacterValues();
		stateTime = 0;
	}

	private void initCharacterValues()
	{
		dimension.set(1, 1);
		// Center image on game object
		origin.set(dimension.x / 2, dimension.y / 2);
		// Bounding box for collision detection
		bounds.set(position.x, position.y, dimension.x, dimension.y);
		// Set physics values
		maxVelocityWalking.set(1.5f, 2.0f);// (3.0f, 4.0f);
		// View direction
		playerIsLooking = PLAYER_IS_LOOKING.RIGHT;
		// Jump state
		playerState = PLAYER_STATE.STANDING;
	}

	private void splitSpritesheetAndCreateAnimations()
	{
		// wczytuje texture i dzieli na kawalki spritesheet
		TextureRegion[][] temp = gamerAtlasRegion.split(gamerAtlasRegion.getRegionWidth() / Player.rowsInSpritesheet, gamerAtlasRegion.getRegionHeight()
				/ Player.colsInSpritesheet);
		TextureRegion[] throwingTemp =
		{temp[1][4], temp[1][5], temp[1][6], temp[1][7], temp[1][6], temp[1][5]};
		throwingAnimation = new Animation(0.07f, throwingTemp);
		throwingAnimation.setPlayMode(PlayMode.LOOP);
		dyingAnimation = new Animation(0.155f, temp[2]);
		dyingAnimation.setPlayMode(PlayMode.NORMAL);
		walkingAnimation = new Animation(0.07f, temp[4]);
		walkingAnimation.setPlayMode(PlayMode.LOOP);
		goingUpstairsAnimation = new Animation(0.1f, temp[6]);
		goingUpstairsAnimation.setPlayMode(PlayMode.LOOP);
		goingDownstairsAnimation = new Animation(0.1f, temp[7]);
		goingDownstairsAnimation.setPlayMode(PlayMode.LOOP);
		standingAnimation = new Animation(0.155f, temp[8][0]);
		standingAnimation.setPlayMode(PlayMode.NORMAL);
		jumpFallingAnimation = new Animation(0.155f, temp[5][5], temp[5][6]);
		jumpFallingAnimation.setPlayMode(PlayMode.LOOP);
		jumpRisingAnimation = new Animation(0.155f, temp[5][2], temp[5][3], temp[5][4]);
		jumpRisingAnimation.setPlayMode(PlayMode.NORMAL);
	}

	public void setJumping()
	{
		this.position = body.getPosition();
		this.velocity = body.getLinearVelocity();
		switch (playerState)
		{
			case STANDING :
				body.applyLinearImpulse(0, 4f, this.position.x, this.position.y, true);
				break;
			case WALKING :
				body.applyLinearImpulse(0, 3f, this.position.x, this.position.y, true);
				break;
			// -2 - w lewo do g�ry
			// -1 - w lewo w d�
			// 0 - stoi
			// 1 - w prawo w d�
			// 2 - w prawo do g�ry
			case GOING_DOWNSTAIRS :
				int side = (goingStairs == -1 || goingStairs == -2) ? -1 : 1;
				body.applyLinearImpulse(side * 0.5f, 2f, this.position.x, this.position.y, true);
				break;
			default :
				break;
		}
	};

	@Override
	public void render( SpriteBatch batch )
	{	
		TextureRegion tex = currAnimation.getKeyFrame(stateTime);

		batch.draw(tex.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, tex.getRegionX(),
				tex.getRegionY(), tex.getRegionWidth(), tex.getRegionHeight(), playerIsLooking == PLAYER_IS_LOOKING.LEFT, false);
	
		for (FireBall fb : fireBalls)
		{
			fb.render(batch);
		}
	}

	@Override
	public void update( float deltaTime )
	{
		// update time to animation
		stateTime += deltaTime;
		this.position = body.getPosition();
		this.velocity = body.getLinearVelocity();
		// to not move player too far from map
		position.x = Math.max(-0.5f, position.x);
		position.x = Math.min(49.5f, position.x);
		position.y = Math.max(-1f, position.y);
		position.y = Math.min(20.5f, position.y);
		body.setTransform(position.x, position.y, 0);
		if (platformReference == null)
		{
			body.setGravityScale(1);
			// apply walking
			if (velocity.x != 0)
			{
				playerIsLooking = velocity.x < 0 ? PLAYER_IS_LOOKING.LEFT : PLAYER_IS_LOOKING.RIGHT;
				playerState = PLAYER_STATE.WALKING;
			}
			if (velocity.y != 0)
			{
				playerState = velocity.y > 0 ? PLAYER_STATE.JUMP_RISING : PLAYER_STATE.JUMP_FALLING;
			}
			// -2 - w lewo do g�ry
			// -1 - w lewo w d�
			// 0 - stoi
			// 1 - w prawo w d�
			// 2 - w prawo do g�ry
			// apply going up/downStairs
			if (goingStairs == -1 || goingStairs == 1)
			{
				playerState = PLAYER_STATE.GOING_DOWNSTAIRS;
			}
			else
				if (goingStairs == -2 || goingStairs == 2)
				{
					playerState = PLAYER_STATE.GOING_UPSTAIRS;
					// this.body.setGravityScale(0.0f);
				}
				else
				{
					this.body.setGravityScale(0.5f);
				}
			// apply standing
			if (velocity.x == 0.0f && velocity.y == 0.0f && goingStairs == 0)
			{
				playerState = PLAYER_STATE.STANDING;
			}
		}
		else
		{
			// body.setGravityScale(0);
			if (Gdx.input.isKeyPressed(Keys.LEFT) || GameRenderer.leftButtonOnScreenIsPressed)
			{
				playerIsLooking = PLAYER_IS_LOOKING.LEFT;
				playerState = PLAYER_STATE.WALKING;
			}
			else
				if (Gdx.input.isKeyPressed(Keys.RIGHT) || GameRenderer.rightButtonOnScreenIsPressed)
				{
					playerIsLooking = PLAYER_IS_LOOKING.RIGHT;
					playerState = PLAYER_STATE.WALKING;
				}
				else
				{
					playerState = PLAYER_STATE.STANDING;
				}
		}
		for (FireBall fb : fireBalls)
		{
			fb.update(deltaTime);
		}
		PLAYER_STATE prevPlayerState = playerState;
		// apply fireball throwing
		if (this.throwTime != 0f)
		{
			// animate throwing man
			if (this.throwTime < THROW_TIME_MAX)
			{
				playerState = PLAYER_STATE.THROWING;
			}
			// throw real fireball
			else
			{
				throwTime = 0.0f;
				this.playerState = prevPlayerState;
				for (FireBall fb : fireBalls)
				{
					if (fb.canThrow)
					{
						indexCurrentFireBall = fireBalls.indexOf(fb, false);
						break;
					}
				}
				FireBall fb = fireBalls.get(indexCurrentFireBall);
				fb.body.setActive(true);
				float temp = playerIsLooking == PLAYER_IS_LOOKING.LEFT ? -1f : 1f;
				fb.body.setTransform(position.x + origin.x * temp, position.y + origin.y, 0);
				// reset force after deactivation
				fb.body.setLinearVelocity(0f, 0f);
				// fb.body.setLinearDamping(0f);
				fb.body.setLinearDamping(0.5f);
				if (GamePreferences.getInstance().sound)
				{
					MyAssetsManager.instance.throwing.play(GamePreferences.getInstance().volSound);
				}
				fb.body.applyLinearImpulse(playerIsLooking == PLAYER_IS_LOOKING.LEFT ? -5.0f + this.velocity.x : 5.0f, 2.5f, fb.position.x, fb.position.y, true);
				fb.canThrow = false;
			}
		}
		setAnimation(this.playerState);
	}

	// wczytuje ustawienie typu charakteru i przypisuje odpowiednia texture
	public void loadAndSetCharacterType()
	{
		switch (GamePreferences.getInstance().characterSkin.toUpperCase())
		{
			case "CLOTH" :
				gamerAtlasRegion = MyAssetsManager.instance.textures.get("assets1/Player/clothCharacter");
				break;
			case "LEATHER" :
				gamerAtlasRegion = MyAssetsManager.instance.textures.get("assets1/Player/leatherCharacter");
			
				break;
			case "STEEL" :
				gamerAtlasRegion = MyAssetsManager.instance.textures.get("assets1/Player/steelCharacter");
				break;
			default :
				Gdx.app.error("SOMETHING'S WRONG WITH PLAYER ASSET", "Bad");
				break;
		}
	}

	// set currAnim in dependency of PLAYER_STATE
	public void setAnimation( PLAYER_STATE state )
	{
		switch (state)
		{
			case THROWING :
				currAnimation = throwingAnimation;
				break;
			case WALKING :
				currAnimation = walkingAnimation;
				break;
			case GOING_UPSTAIRS :
				currAnimation = goingUpstairsAnimation;
				break;
			case GOING_DOWNSTAIRS :
				currAnimation = goingDownstairsAnimation;
				break;
			case STANDING :
				currAnimation = standingAnimation;
				break;
			case JUMP_FALLING :
				currAnimation = jumpFallingAnimation;
				break;
			case JUMP_RISING :
				currAnimation = jumpRisingAnimation;
				break;
			default :
				Gdx.app.error("SOMETHING'S WRONG WITH PLAYER setAnimation", "setAnimation");
				break;
		}
	}

	public MovingPlatform getPlatformReference()
	{
		return platformReference;
	}

	public void setPlatformReference( MovingPlatform platformReference )
	{
		this.platformReference = platformReference;
	}
}
