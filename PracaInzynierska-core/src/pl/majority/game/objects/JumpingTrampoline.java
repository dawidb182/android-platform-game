
package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class JumpingTrampoline extends AbstractGameObject
{
	public Animation animation;
	private TextureRegion[] frames;
	private TextureRegion currFrame;
	private float deltaTime;
	public boolean canThrowPlayer;
	private Player playerReference;
	private Vector2 forceToMovePlayer;

	public JumpingTrampoline( int x, int y )
	{
		forceToMovePlayer = new Vector2(0.0f, 5.0f);
		deltaTime = 1.0f;
		canThrowPlayer = true;
		this.position.x = x;
		this.position.y = y;
		dimension.set(1, 1);
		bounds.set(this.position.x, this.position.y, this.dimension.x, this.dimension.y / 2);
		frames = new TextureRegion[2];
		frames[0] = new TextureRegion(MyAssetsManager.instance.textures.get("assets1/Platforms/springboardDown"));
		frames[1] = new TextureRegion(MyAssetsManager.instance.textures.get("assets1/Platforms/springboardUp"));
		currFrame = frames[0];
	}

	@Override
	public void update( float deltaTime )
	{
		if ( playerReference != null )
		{
			playerReference.body.setLinearVelocity(0f, 0f);
			playerReference.body.setAngularVelocity(0);
			
			playerReference.body.applyLinearImpulse(forceToMovePlayer, playerReference.body.getPosition(), true);
			currFrame = frames[1];
			this.deltaTime = 0.5f;
			playerReference = null;
		}
		else
		{
			if((this.deltaTime -= deltaTime) < 0)
			{
				currFrame = frames[0];
			}
		}
	}

	@Override
	public void render( SpriteBatch batch )
	{
		batch.draw(currFrame, position.x, position.y, dimension.x, dimension.y);
	}

	public void setPlayerReference( Player p )
	{
		this.playerReference = p;
	}
}
