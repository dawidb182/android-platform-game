package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class FinalPoint extends Tile
{

	public FinalPoint( int posX , int posY )
	{
		super("signExit", posX, posY);
	}

	@Override
	public void render(SpriteBatch batch)
	{
		super.render(batch);
	}

	@Override
	public void update(float deltaTime)
	{
		super.update(deltaTime);
	}

}
