
package pl.majority.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

public class FireBall extends AbstractGameObject
{
	public int damage;
	public float rotate;
	public boolean canThrow;
	private TextureRegion currentFrame;

	public FireBall()
	{
		currentFrame = MyAssetsManager.instance.textures.get("assets1/Other/fireball");
		// canThrow = true;
		damage = 50;
		this.position.set(1, 1);
		this.dimension.set(0.7f, 0.7f);
	}

	@Override
	public void render( SpriteBatch batch )
	{
		if (!this.canThrow)
		{
			batch.draw(currentFrame, position.x, position.y, dimension.x, dimension.y);
		}
	}

	@Override
	public void update( float deltaTime )
	{
		if (!this.canThrow)
		{
			super.update(deltaTime);
			velocity.set(body.getLinearVelocity());
			this.bounds.set(position.x, position.y, dimension.x, dimension.y);
			if ((this.velocity.x == 0 && this.velocity.y == 0) || isUnderGround(this.body))
			{
				this.canThrow = true;
				this.body.setActive(false);
			}
		}
	}

	public int getDamage()
	{
		return this.damage;
	}

	public boolean isUnderGround( Body b )
	{
		return b.getPosition().y < -2 || b.getPosition().x < -2;
	}
}
