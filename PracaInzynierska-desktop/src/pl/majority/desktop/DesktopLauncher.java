
package pl.majority.desktop;

import pl.majority.game.MajorityGame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

public class DesktopLauncher
{
	private static boolean rebuild = false;

	public static void main( String[] arg )
	{
		if (rebuild)
		{
			Settings settings = new Settings();
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			TexturePacker.process(settings, "assets-raw/images", "../android/assets/images", "majority.atlas");
		}
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Majority desktop";
		config.width = 800;
		config.height = 480;
		config.resizable = false;
		new LwjglApplication(new MajorityGame(), config);
	}
}
