
![1.png](https://bitbucket.org/repo/nA4djk/images/2067378513-1.png)

The game was created as a *Engineering Thesis* in *Białystok University of Technology*.



This is a simple platform game like Super Mario or Braid, called **Majority**. The game was written using **libGDX** framework and **Box2D** physics engine.

![2.png](https://bitbucket.org/repo/nA4djk/images/2004685454-2.png)

The game allows player to navigate through a two-dimensional (side view of character) game world, jumping on the platforms (including mobile platforms), avoiding and destroying enemies, also a player may collect coins.

Platform editor has been created as a external tool for the game that allows you to design new levels.

![4.png](https://bitbucket.org/repo/nA4djk/images/1573517631-4.png)

Especially for this game has been invented musicg library what gives a chance for sound analysis. It allows whistle detection among other things.

![3.png](https://bitbucket.org/repo/nA4djk/images/2932815014-3.png)